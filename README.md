# AppExpenses_Android

## Layers 
- ### App
	#### To develop the Mobil App was used Android Studio where was implemented the following resources:
		- NavigationDrawer to put the options menu.
		- Fragments	to the diferents screen reusing the same activity. 
		- Activities to Login and Sign In Activity.
		- ReclyclerView to show de records on the diferents fragments.
		- Cardview to change the style of the records from RecyclerView.
		- Volley to can connect the App to the database remote using WebApi REST.
		- Adapters to load and put the data on the RecyclerView on the diferents Fragments.
		- SQLite to save the user data after log in using user and password to save the data locally int eh device.
		

- ### WebApi REST C#
	#### To create the Web Api it was used .NET, the Web Api was divide in the following layers:
		- Core Layer
			### 
		- Data Layer
			### This layer has the responsibility to conect the Web Api to database used EntityFramework Code First and migration.
			### Also this layer has the methods to the CRUD operations.
			### 
		- Api Layer
			### This layer has the HTTP methods (HttpGet, HttpPost, HttpPut, HttpDelete).
		
		
![alt tag](https://bitbucket.org/bsn12993/appexpenses_android/src/master/Images/Screenshot_20190730-001052.png)


