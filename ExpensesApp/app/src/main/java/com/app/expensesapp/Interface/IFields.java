package com.app.expensesapp.Interface;

import android.view.View;

public interface IFields {
    void CleanFields();
    void InitComponents();
    void InitComponents(View view);
    void ButtonEvents();
    boolean ValidateFields();

}
