package com.app.expensesapp.Util;

import android.content.Context;

import com.app.expensesapp.R;

public class Config {

    private Context context;
    private static Config instance;

    public static Config getInstance(Context context){
        if(instance==null)return  new Config(context);
        else return instance;
    }

    private Config (Context ctx){
        context=ctx;
    }

    private static String endPoint;

    public String getEndPoint() { return context.getString(R.string.endPoint); }

    public String getSharedPreferences(){return context.getString(R.string.sharedPreferences_name);}
    public String getUrlWSValidateUser(){return getEndPoint()+context.getString(R.string.ws_validateUser);}
    public String getUrlWSUserById(){return getEndPoint()+context.getString(R.string.ws_userGetById);}
    public String getUrlWSExpensesByIdUser(){return getEndPoint()+context.getString(R.string.ws_expensesByUser);}
    public String getUrlWSIncomesByIdUser(){return getEndPoint()+context.getString(R.string.ws_incomesByUser);}
    public String getUrlWSExpensesCategoryTotal(){return  getEndPoint()+context.getString(R.string.ws_expensesCategoryTotal);}

    public String getUrlWSUserCreate(){return getEndPoint()+context.getString(R.string.ws_createUser);}
    public String getUrlWsCreateCategoryByUser(){return getEndPoint()+context.getString(R.string.ws_createCategoryByUser);}
    public String getUrlWsCreateExpenseByUser(){return getEndPoint()+context.getString(R.string.ws_createExpenseByUser);}
    public String getUrlWsCreateIncomeByUser(){return getEndPoint()+context.getString(R.string.ws_createIncomeByUser);}

    public String getUrlWsUpdateUser(){return getEndPoint()+context.getString(R.string.ws_updateUser);}
    public String getUrlWsUpdateUserName(){return getEndPoint()+context.getString(R.string.ws_updateUserName);}
    public String getUrlWsUpdateUserLastName(){return getEndPoint()+context.getString(R.string.ws_updateUserLastName);}
    public String getUrlWsUpdateUserEmail(){return getEndPoint()+context.getString(R.string.ws_updateUserEmail);}
    public String getUrlWsUpdateUserPassword(){return getEndPoint()+context.getString(R.string.ws_updateUserPassword);}

    public String getUrlWsUpdateExpense(){return getEndPoint()+context.getString(R.string.ws_updateExpense);}
    public String getUrlWsUpdateCategory(){return getEndPoint()+context.getString(R.string.ws_updateCategory);}
    public String getUrlWsUpdateIncome(){return getEndPoint()+context.getString(R.string.ws_updateIncome);}

    public String getUrlWsDeleteUserCategory(){return getEndPoint()+context.getString(R.string.ws_deleteUserCategory);}
    public String getUrlWsDeleteCategory(){return getEndPoint()+context.getString(R.string.ws_deleteCategory);}
    public String getUrlWsDeleteExpense(){return getEndPoint()+context.getString(R.string.ws_deleteExpense);}
    public String getUrlWsDeleteIncome(){return getEndPoint()+context.getString(R.string.ws_deleteIncome);}

    public String getTextFieldName(){return context.getString(R.string.field_name);}
    public String getTextFieldLastName(){return context.getString(R.string.field_lastname);}
    public String getTextFieldEmail(){return context.getString(R.string.field_email);}
    public String getTextFieldPassword(){return context.getString(R.string.field_password);}

    public String getMessageInfo(){return context.getString(R.string.noRecord);}
    public String getMessageNoInternetConnection(){return context.getString(R.string.noInternetConnection);}



}
