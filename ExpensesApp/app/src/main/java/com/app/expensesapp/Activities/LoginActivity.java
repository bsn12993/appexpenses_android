package com.app.expensesapp.Activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.expensesapp.Interface.IFields;
import com.app.expensesapp.Models.Response;
import com.app.expensesapp.Models.User;
import com.app.expensesapp.R;
import com.app.expensesapp.SQLite.SQLiteConnection;
import com.app.expensesapp.SQLite.Schema;

import com.app.expensesapp.Util.Config;
import com.app.expensesapp.Util.Security;
import com.app.expensesapp.Util.SharedPreferencesUser;
import com.app.expensesapp.Util.VolleySingleton;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class LoginActivity extends AppCompatActivity
        implements
        IFields {

    /**
     * Componentes
     */
    private Button btn_login;
    private Button btn_register;
    private EditText txt_user;
    private EditText txt_password;
    private android.support.v7.app.AlertDialog.Builder builder;
    private ProgressBar pb_login;

    /**
     * Objetos
     */
    private JsonObjectRequest jsonObjectRequest;
    private Response _Response;
    private Gson gson;
    private SQLiteConnection connection;
    private SharedPreferencesUser preferencesUser;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        InitComponents();
        gson=new Gson();

        setTitle(R.string.title_login);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        preferencesUser=new SharedPreferencesUser(this);

        connection=new SQLiteConnection(this, Schema.Database_Name,null,1);
        User user=connection.SelectUser(connection);
        RedirectActivity(user);
        ButtonEvents();
    }

    public void Get(String url) {
        jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url, null,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        pb_login.setVisibility(View.INVISIBLE);
                        _Response = gson.fromJson(response.toString(),Response.class);
                        String json=gson.toJson(_Response.getResult());
                        Intent iHome=new Intent(LoginActivity.this,MainActivity.class);
                        iHome.putExtra("json",json);
                        startActivity(iHome);
                        finish();
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pb_login.setVisibility(View.INVISIBLE);

                        int code=error.networkResponse.statusCode;

                        try {
                            String s=new String(error.networkResponse.data,
                                    HttpHeaderParser.parseCharset(error.networkResponse.headers));
                            _Response=gson.fromJson(s,Response.class);
                            Toast.makeText(getApplicationContext(),_Response.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        } catch (UnsupportedEncodingException e1) {
                            e1.printStackTrace();
                        }
                    }
                });
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    private void RedirectActivity(User user){
        if(user!=null){
            String json=gson.toJson(user);
            Intent iMain=new Intent(LoginActivity.this,MainActivity.class);
            iMain.putExtra("json",json);
            startActivity(iMain);
            finish();
            return;
        }
    }

    @Override
    public void CleanFields() {

    }

    @Override
    public void InitComponents() {
        pb_login = (ProgressBar) findViewById(R.id.pb_login);
        txt_user=(EditText)findViewById(R.id.txt_user);
        txt_password=(EditText)findViewById(R.id.txt_password);
        btn_register=(Button)findViewById(R.id.btn_register);
        btn_login = (Button) findViewById(R.id.btn_login);
        pb_login=(ProgressBar)findViewById(R.id.pb_login);

    }

    @Override
    public void InitComponents(View view) {

    }

    @Override
    public void ButtonEvents() {
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iRegister=new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(iRegister);
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                if(!ValidateFields()){
                    Toast.makeText(getApplicationContext(),
                            "Campos requeridos",Toast.LENGTH_LONG).show();
                    return;

                }
                String pw= Security.getCifrado(txt_password.getText().toString(),"SHA1");
                String url=Config.getInstance(getApplicationContext()).getUrlWSValidateUser();
                url=url.replace("{email}",txt_user.getText().toString());
                url=url.replace("{password}",pw);
                pb_login.setVisibility(View.VISIBLE);
                Get(url);
            }
        });
    }

    @Override
    public boolean ValidateFields() {
        if(!txt_user.getText().toString().equals("") &&
            !txt_password.getText().toString().equals(""))return true;
        else return false;
    }
}


