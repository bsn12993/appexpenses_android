package com.app.expensesapp.Implement;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.expensesapp.Models.Response;
import com.app.expensesapp.Models.User;
import com.app.expensesapp.Interface.ApiServices;
import com.app.expensesapp.Util.VolleySingleton;

import org.json.JSONObject;

public class UserService extends VolleyData implements ApiServices<User> {

    public static UserService instance;
    private Context context;

    private UserService(Context context) {
        this.context=context;
    }

    public static UserService getInstance(Context context){
        if(instance==null){
            instance=new UserService(context);
        }
        return instance;
    }


    @Override
    public Response Get(String url) {
        responseData=new Response();
        jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url, null,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        responseData=gson.fromJson(response.toString(),Response.class);
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        responseData.setMessage(error.toString());
                    }
                });
        VolleySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
        return responseData;
    }

    @Override
    public Response GetById(String url) {
        return null;
    }

    @Override
    public Response Post(String url, User model) {
        return null;
    }

    @Override
    public Response Put(String url, User model) {
        return null;
    }

    @Override
    public Response Delete(String url) {
        return null;
    }
}
