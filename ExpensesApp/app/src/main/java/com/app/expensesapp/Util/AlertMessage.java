package com.app.expensesapp.Util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentTransaction;

public class AlertMessage extends AlertDialog {

    private static AlertMessage instance;
    private AlertDialog.Builder builder;
    private AlertDialog dialog;
    private Context context;

    private AlertMessage(Context ctx) {
        super(ctx);
        this.context=ctx;
    }

    public static AlertMessage getInstance(Context context){
        if(instance==null)return new AlertMessage(context);
        else return instance;
    }

    public void ShowMessage(String title, String message){
        builder=new Builder(this.context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setNeutralButton("OK", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog=builder.create();
        dialog.show();
    }

    public void ShowMessage(String title, String message, final FragmentTransaction transaction){
        builder=new Builder(this.context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setNeutralButton("OK", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                transaction.commit();
            }
        });
        dialog=builder.create();
        dialog.show();
    }

    public void hideAlertDialog(){
        dialog.hide();
    }
}
