package com.app.expensesapp.Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.expensesapp.Adapters.ExpensesAdapter;
import com.app.expensesapp.Interface.IFields;
import com.app.expensesapp.SQLite.SQLiteConnection;
import com.app.expensesapp.SQLite.Schema;
import com.app.expensesapp.Util.Config;
import com.app.expensesapp.Interface.IApiService;
import com.app.expensesapp.Models.Expense;
import com.app.expensesapp.Models.Response;
import com.app.expensesapp.R;
import com.app.expensesapp.Util.AlertMessage;
import com.app.expensesapp.Util.Connectivity;
import com.app.expensesapp.Util.VolleySingleton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ExpensesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ExpensesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ExpensesFragment extends Fragment implements
        IFields {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Components
     */
    private RecyclerView rvExpenses;
    private ProgressBar pb_expenses;
    private FloatingActionButton fabExpenseItem;
    private TextView txt_noRecordExpense;
    private FloatingActionButton fab_reloadExpenses;
    private ImageView img_noInternetConnectionExpense;

    /**
     * Objects
     */
    private JsonObjectRequest jsonObjectRequest;
    private Gson gson;
    private List<Expense> expenseList;
    private NetworkInfo networkInfo;
    private SQLiteConnection connection;

    /**
     * Fragments
     */
    private FragmentTransaction transaction;
    private ExpenseItemFragment itemFragment;

    public ExpensesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ExpensesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ExpensesFragment newInstance(String param1, String param2) {
        ExpensesFragment fragment = new ExpensesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_expenses, container, false);
        gson=new Gson();
        InitComponents(view);
        ButtonEvents();

        networkInfo=Connectivity.getInstance(getContext()).CheckInternetConnection();
        if(networkInfo==null || !networkInfo.isConnected()){
            pb_expenses.setVisibility(View.INVISIBLE);
            img_noInternetConnectionExpense.setVisibility(View.VISIBLE);
            Toast.makeText(getContext(),Config.getInstance(getContext()).getMessageNoInternetConnection(),Toast.LENGTH_LONG).show();
        }
        else {
            pb_expenses.setVisibility(View.VISIBLE);
            connection=new SQLiteConnection(getContext(), Schema.Database_Name,null,1);
            int idUser=connection.SelectIDUser(connection);
            String url=Config.getInstance(getContext()).getUrlWSExpensesByIdUser();
            url=url.replace("{iduser}",idUser+"");
            Get(url, getContext());
        }

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void Get(String url, Context context) {
        jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url, null,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        pb_expenses.setVisibility(View.INVISIBLE);
                        txt_noRecordExpense.setText("");
                        Response res=gson.fromJson(response.toString(),Response.class);
                        Type expensesListType=new TypeToken<ArrayList<Expense>>(){}.getType();
                        String json=gson.toJson(res.getResult());
                        expenseList=gson.fromJson(json,expensesListType);
                        itemFragment=new ExpenseItemFragment();
                        transaction=getFragmentManager().beginTransaction();
                        ExpensesAdapter expensesAdapter=new ExpensesAdapter(expenseList,transaction,itemFragment,getContext());
                        rvExpenses.setAdapter(expensesAdapter);
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pb_expenses.setVisibility(View.INVISIBLE);
                        if(error.networkResponse!=null){
                            try {
                                String s=new String(error.networkResponse.data,
                                        HttpHeaderParser.parseCharset(error.networkResponse.headers));
                                com.app.expensesapp.Models.Response _Response=gson
                                        .fromJson(s, com.app.expensesapp.Models.Response.class);
                                AlertMessage.getInstance(getContext())
                                        .ShowMessage("Informacion",_Response.getMessage());
                            } catch (UnsupportedEncodingException e1) {
                                e1.printStackTrace();
                            }
                        }
                        else {
                            AlertMessage.getInstance(getContext())
                                    .ShowMessage("Informacion",error.toString());
                            txt_noRecordExpense.setText(Config.getInstance(getContext()).getMessageInfo());
                        }
                     }
                });
        VolleySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    public void CleanFields() {

    }

    @Override
    public void InitComponents() {

    }

    @Override
    public void InitComponents(View view) {
        rvExpenses=(RecyclerView)view.findViewById(R.id.rvExpense);
        pb_expenses=(ProgressBar)view.findViewById(R.id.pb_expenses);
        txt_noRecordExpense=(TextView)view.findViewById(R.id.txt_noRecordExpense);
        img_noInternetConnectionExpense=(ImageView)view.findViewById(R.id.img_noInternetConnectionExpense);
        rvExpenses.setLayoutManager(new LinearLayoutManager(getContext()));
        fabExpenseItem=(FloatingActionButton)view.findViewById(R.id.fab_expenseItem);
        fab_reloadExpenses=(FloatingActionButton)view.findViewById(R.id.fab_reloadExpenses);
    }

    @Override
    public void ButtonEvents() {
        fabExpenseItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemFragment=new ExpenseItemFragment();
                transaction=getFragmentManager().beginTransaction();
                transaction.replace(R.id.frameContainer,itemFragment);
                transaction.commit();
            }
        });


        fab_reloadExpenses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_noRecordExpense.setText("");
                img_noInternetConnectionExpense.setVisibility(View.INVISIBLE);
                networkInfo=Connectivity.getInstance(getContext()).CheckInternetConnection();
                if(networkInfo==null || !networkInfo.isConnected()) {
                    img_noInternetConnectionExpense.setVisibility(View.VISIBLE);
                    Toast.makeText(getContext(),Config.getInstance(getContext()).getMessageNoInternetConnection(),Toast.LENGTH_LONG).show();
                    return;
                }

                connection=new SQLiteConnection(getContext(), Schema.Database_Name,null,1);
                int idUser=connection.SelectIDUser(connection);
                String url=Config.getInstance(getContext()).getUrlWSExpensesByIdUser();
                url=url.replace("{iduser}",idUser+"");
                pb_expenses.setVisibility(View.VISIBLE);
                Get(url, getContext());
            }
        });
    }

    @Override
    public boolean ValidateFields() {
        return false;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
