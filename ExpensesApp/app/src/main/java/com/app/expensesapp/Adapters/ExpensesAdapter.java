package com.app.expensesapp.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.app.expensesapp.Fragments.ExpenseItemFragment;
import com.app.expensesapp.Models.Expense;
import com.app.expensesapp.Models.UserCategory;
import com.app.expensesapp.R;
import com.app.expensesapp.Util.Config;
import com.app.expensesapp.Util.Util;
import com.app.expensesapp.Util.VolleySingleton;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class ExpensesAdapter extends RecyclerView.Adapter<ExpensesAdapter.ViewHolderExpenses> {

    List<Expense> expenseList=new ArrayList<>();
    FragmentTransaction transaction;
    Fragment fragment;
    Context context;

    public ExpensesAdapter(List<Expense> expenseList, FragmentTransaction transaction, Fragment fragment, Context context){
        this.expenseList=expenseList;
        this.transaction=transaction;
        this.fragment=fragment;
        this.context=context;
    }

    @NonNull
    @Override
    public ViewHolderExpenses onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_expense,null,false);
        return new ViewHolderExpenses(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderExpenses viewHolderExpenses, int i) {
        viewHolderExpenses.setExpense(expenseList.get(i));
    }

    @Override
    public int getItemCount() {
        return expenseList.size();
    }

    public class ViewHolderExpenses extends RecyclerView.ViewHolder {

        TextView ExpenseCategory;
        TextView ExpenseAmount;
        TextView ExpenseDate;
        Button btn_editExpense;
        Button btn_deleteExpense;
        CardView cv_expense;
        Bundle bundle;

        public ViewHolderExpenses(@NonNull View itemView) {
            super(itemView);
            ExpenseCategory=(TextView)itemView.findViewById(R.id.txt_expenseCategory);
            ExpenseAmount=(TextView)itemView.findViewById(R.id.txt_expenseAmount);
            ExpenseDate=(TextView)itemView.findViewById(R.id.txt_expenseDate);
            btn_editExpense=(Button)itemView.findViewById(R.id.btn_editExpense);
            cv_expense=(CardView)itemView.findViewById(R.id.cv_expense);

        }

        public void setExpense(Expense item) {
            cv_expense.setTag(item.getExpense_Id()+"-"+item.getCategory_Id());
            ExpenseAmount.setText(String.valueOf(item.getAmount()));
            ExpenseDate.setText(Util.setFormatDate(item.getDate()));
            ExpenseCategory.setText(item.getCategory().getName());
            btn_editExpense.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bundle=new Bundle();
                    bundle.putString("expense_id",cv_expense.getTag().toString().split("-")[0]);
                    bundle.putString("amount",ExpenseAmount.getText().toString());
                    bundle.putString("date",ExpenseDate.getText().toString());
                    bundle.putString("category_id",cv_expense.getTag().toString().split("-")[1]);
                    bundle.putString("category",ExpenseCategory.getText().toString());
                    fragment.setArguments(bundle);
                    transaction.replace(R.id.frameContainer,fragment).addToBackStack(null);
                    transaction.commit();
                }
            });



        }
    }
}
