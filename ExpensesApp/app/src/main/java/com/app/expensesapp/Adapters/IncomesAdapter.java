package com.app.expensesapp.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.app.expensesapp.Models.Expense;
import com.app.expensesapp.Models.Income;
import com.app.expensesapp.R;
import com.app.expensesapp.Util.Config;
import com.app.expensesapp.Util.Util;
import com.app.expensesapp.Util.VolleySingleton;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class IncomesAdapter extends RecyclerView.Adapter<IncomesAdapter.ViewHolderIncomes> {

    List<Income> incomeList=new ArrayList<>();
    Fragment fragment;
    FragmentTransaction transaction;
    Context context;

    public IncomesAdapter(List<Income> incomeList, FragmentTransaction transaction, Fragment fragment, Context context){
        this.incomeList=incomeList;
        this.fragment=fragment;
        this.transaction=transaction;
        this.context=context;
    }

    @NonNull
    @Override
    public ViewHolderIncomes onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_income,null,false);
        return new ViewHolderIncomes(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderIncomes viewHolderIncomes, int i) {
        viewHolderIncomes.setIncomes(incomeList.get(i));
    }

    @Override
    public int getItemCount() {
        return incomeList.size();
    }

    public class ViewHolderIncomes extends RecyclerView.ViewHolder {

        TextView IncomeAmount;
        TextView IncomeDate;
        CardView cv_incomes;
        Button btn_editIncome;
        Bundle bundle;

        public ViewHolderIncomes(@NonNull final View itemView) {
            super(itemView);

            IncomeAmount=(TextView)itemView.findViewById(R.id.txt_incomeAmout);
            IncomeDate=(TextView)itemView.findViewById(R.id.txt_incomeDate);
            cv_incomes=(CardView)itemView.findViewById(R.id.cv_incomes);
            btn_editIncome=(Button)itemView.findViewById(R.id.btn_editIncome);
            btn_editIncome.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bundle=new Bundle();
                    bundle.putString("income_id",cv_incomes.getTag().toString());
                    bundle.putString("amount",IncomeAmount.getText().toString());
                    bundle.putString("date",IncomeDate.getText().toString());
                    fragment.setArguments(bundle);
                    transaction.replace(R.id.frameContainer,fragment).addToBackStack(null).commit();
                }
            });
        }

        public void setIncomes(Income item) {
            cv_incomes.setTag(item.getIncome_Id());
            IncomeAmount.setText(String.valueOf(item.getAmount()));
            IncomeDate.setText(Util.setFormatDate(item.getDate()));
        }
    }
}
