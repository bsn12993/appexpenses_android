package com.app.expensesapp.Models;

import com.app.expensesapp.Util.Components;

import java.util.Date;

public class Income extends Components {

    private int Income_Id;
    private String Date;
    private double Amount;
    private int User_Id;
    private User User;

    public Income() {
    }

    public int getIncome_Id() {
        return Income_Id;
    }

    public void setIncome_Id(int income_Id) {
        Income_Id = income_Id;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double amount) {
        Amount = amount;
    }

    public int getUser_Id() {
        return User_Id;
    }

    public void setUser_Id(int user_Id) {
        User_Id = user_Id;
    }

    public com.app.expensesapp.Models.User getUser() {
        return User;
    }

    public void setUser(com.app.expensesapp.Models.User user) {
        User = user;
    }

    @Override
    public void ClearFields() {
        setDate("");
        setAmount(0);
        setUser_Id(0);
    }

    @Override
    public boolean Validate() {
        if(getAmount()!=0 &&
        !getDate().equals(""))return true;
        else return false;
    }

    public boolean IsValidAmount(){
        return (getAmount()!=0)?true:false;
    }

    public boolean IsValidDate(){
        return (!getDate().equals(""))?true:false;
    }
}
