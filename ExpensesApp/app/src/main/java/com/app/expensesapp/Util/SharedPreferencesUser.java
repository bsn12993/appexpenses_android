package com.app.expensesapp.Util;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;

import com.app.expensesapp.Models.User;

public class SharedPreferencesUser extends ContextWrapper {

    private SharedPreferences preferences;
    private Context context;

    public SharedPreferencesUser(Context base) {
        super(base);
        context=base;
    }

    public void savePreferences(User user){
        preferences=getSharedPreferences(Config.getInstance(context).getSharedPreferences(),Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=preferences.edit();
        editor.putInt("iduser",user.getUser_Id());
        editor.putString("email",user.getEmail());
        editor.commit();
    }

    public User getPreferences(){
        User user=null;
        preferences=getSharedPreferences(Config.getInstance(context).getSharedPreferences(),Context.MODE_PRIVATE);
        user.setUser_Id(preferences.getInt("iduser",0));
        user.setEmail(preferences.getString("email",""));
        return user;
    }
}
