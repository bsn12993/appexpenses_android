package com.app.expensesapp.Fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.app.expensesapp.Interface.IApiService;
import com.app.expensesapp.Interface.IFields;
import com.app.expensesapp.Models.Income;
import com.app.expensesapp.R;
import com.app.expensesapp.SQLite.SQLiteConnection;
import com.app.expensesapp.SQLite.Schema;
import com.app.expensesapp.Util.AlertMessage;
import com.app.expensesapp.Util.Config;
import com.app.expensesapp.Util.Connectivity;
import com.app.expensesapp.Util.Util;
import com.app.expensesapp.Util.VolleySingleton;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link IncomeItemFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link IncomeItemFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class IncomeItemFragment extends Fragment implements
        IFields {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Components
     */
    private Button btn_selectDate;
    private EditText txt_dateIncome;
    private EditText txt_incomeAmout;
    private Button btn_createIncome;
    private ProgressBar pb_createIncome;
    private Button btn_deleteIncome;

    /**
     *
     */
    private int day;
    private int month;
    private int year;

    /**
     * Objects
     */
    private Income income;
    private Calendar calendar;
    private Bundle bundle;
    private Gson gson=new Gson();
    private NetworkInfo networkInfo;
    /**
     * Fragments
     */
    private IncomesFragment incomesFragment;
    private FragmentTransaction transaction;

    public IncomeItemFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment IncomeItemFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static IncomeItemFragment newInstance(String param1, String param2) {
        IncomeItemFragment fragment = new IncomeItemFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_income_item, container, false);
        income=new Income();
        InitComponents(view);
        ButtonEvents();

        bundle=getArguments();
        if(bundle!=null){
            int id=Integer.parseInt(bundle.getString("income_id"));
            String amount=bundle.getString("amount");
            String date=bundle.getString("date");

            date=date.split("T")[0];
            txt_dateIncome.setText(date);
            txt_incomeAmout.setText(amount);

            income.setIncome_Id(id);
            income.setAmount(Double.parseDouble(amount));
            income.setDate(date);

            btn_deleteIncome.setVisibility(View.VISIBLE);
        }

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void Post(String url) {
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pb_createIncome.setVisibility(View.INVISIBLE);

                        com.app.expensesapp.Models.Response res=gson
                                .fromJson(response, com.app.expensesapp.Models.Response.class);
                        AlertMessage.getInstance(getContext())
                                .ShowMessage("Informacion",res.getMessage());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pb_createIncome.setVisibility(View.INVISIBLE);
                        if(error.networkResponse!=null){
                            try {
                                String s=new String(error.networkResponse.data,
                                        HttpHeaderParser.parseCharset(error.networkResponse.headers));
                                com.app.expensesapp.Models.Response _Response=gson
                                        .fromJson(s, com.app.expensesapp.Models.Response.class);
                                AlertMessage.getInstance(getContext())
                                        .ShowMessage("Informacion",_Response.getMessage());
                            } catch (UnsupportedEncodingException e1) {
                                e1.printStackTrace();
                            }
                        }
                        else {
                            AlertMessage.getInstance(getContext())
                                    .ShowMessage("Informacion",error.toString());
                        }
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map=new HashMap<>();
                map.put("Date",income.getDate());
                map.put("Amount",String.valueOf(income.getAmount()));
                map.put("User_Id",String.valueOf(income.getUser_Id()));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
    }

    public void Put(String url) {
        StringRequest stringRequest=new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pb_createIncome.setVisibility(View.INVISIBLE);
                        Gson gson=new Gson();
                        com.app.expensesapp.Models.Response res=gson
                                .fromJson(response, com.app.expensesapp.Models.Response.class);
                        incomesFragment=new IncomesFragment();
                        transaction=getFragmentManager().beginTransaction()
                                .replace(R.id.frameContainer,incomesFragment);
                        AlertMessage.getInstance(getContext())
                                .ShowMessage("Informacion",res.getMessage(),transaction);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pb_createIncome.setVisibility(View.INVISIBLE);
                        if(error.networkResponse!=null){
                            try {
                                String s=new String(error.networkResponse.data,
                                        HttpHeaderParser.parseCharset(error.networkResponse.headers));
                                com.app.expensesapp.Models.Response _Response=gson
                                        .fromJson(s, com.app.expensesapp.Models.Response.class);
                                AlertMessage.getInstance(getContext())
                                        .ShowMessage("Informacion",_Response.getMessage());
                            } catch (UnsupportedEncodingException e1) {
                                e1.printStackTrace();
                            }
                        }
                        else {
                            AlertMessage.getInstance(getContext())
                                    .ShowMessage("Informacion",error.toString());
                        }
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map=new HashMap<>();
                map.put("Income_Id",income.getIncome_Id()+"");
                map.put("Date", Util.setFormatDateEN(income.getDate()));
                map.put("Amount",String.valueOf(income.getAmount()));
                map.put("User_Id",String.valueOf(income.getUser_Id()));
                return map;
            }
        };
        stringRequest
                .setRetryPolicy(new DefaultRetryPolicy(
                        DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
    }

    @Override
    public void CleanFields() {

    }

    @Override
    public void InitComponents() {

    }

    @Override
    public void InitComponents(View view) {
        pb_createIncome=view.findViewById(R.id.pb_createIncome);
        txt_dateIncome=view.findViewById(R.id.txt_dateIncome);
        txt_incomeAmout=view.findViewById(R.id.txt_incomeAmout);
        btn_selectDate=view.findViewById(R.id.btn_selectDateIncome);
        btn_createIncome=view.findViewById(R.id.btn_createIncome);
        btn_deleteIncome=view.findViewById(R.id.btn_deleteIncome);
    }

    @Override
    public void ButtonEvents() {
        btn_selectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar=Calendar.getInstance();
                day=calendar.get(Calendar.DAY_OF_MONTH);
                month=calendar.get(Calendar.MONTH);
                year=calendar.get(Calendar.YEAR);

                DatePickerDialog pickerDialog=new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        txt_dateIncome.setText(dayOfMonth+"/"+(month+1)+"/"+year);
                        income.setDate(year+"/"+(month+1)+"/"+dayOfMonth);
                    }
                },year,month,day);
                pickerDialog.show();
            }
        });

        btn_createIncome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ValidateFields()){
                    Toast.makeText(getContext(),"Campos Vacios",Toast.LENGTH_LONG).show();
                    return;
                }
                SQLiteConnection connection=new SQLiteConnection(getContext(),
                        Schema.Database_Name,null,1);
                int idUser=connection.SelectIDUser(connection);
                income.setAmount(Double.parseDouble(txt_incomeAmout.getText().toString()));
                //income.setDate(txt_dateIncome.getText().toString());
                income.setUser_Id(idUser);

                networkInfo= Connectivity.getInstance(getContext()).CheckInternetConnection();
                if(networkInfo==null || !networkInfo.isConnected()){
                    AlertMessage.getInstance(getContext())
                            .ShowMessage("Informacion",Config.getInstance(getContext())
                                    .getMessageNoInternetConnection());
                    return;
                }

                pb_createIncome.setVisibility(View.VISIBLE);
                if(income.getIncome_Id()==0){
                    Post(Config.getInstance(getContext()).getUrlWsCreateIncomeByUser());
                }
                else {
                    String url=Config.getInstance(getContext()).getUrlWsUpdateIncome();
                    url=url.replace("{id}",income.getIncome_Id()+"");
                    Put(url);
                }
            }
        });

        btn_deleteIncome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                networkInfo= Connectivity.getInstance(getContext()).CheckInternetConnection();
                if(networkInfo==null || !networkInfo.isConnected()){
                    AlertMessage.getInstance(getContext())
                            .ShowMessage("Informacion",Config.getInstance(getContext())
                                    .getMessageNoInternetConnection());
                    return;
                }
                pb_createIncome.setVisibility(View.VISIBLE);
                String url=Config.getInstance(getContext()).getUrlWsDeleteIncome();
                url=url.replace("{id}",income.getIncome_Id()+"");
                StringRequest stringRequest=new StringRequest(Request.Method.DELETE, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                pb_createIncome.setVisibility(View.INVISIBLE);
                                Gson gson=new Gson();
                                com.app.expensesapp.Models.Response res=gson.fromJson(response,
                                        com.app.expensesapp.Models.Response.class);
                                incomesFragment=new IncomesFragment();
                                transaction=getFragmentManager().beginTransaction()
                                        .replace(R.id.frameContainer,incomesFragment);
                                AlertMessage.getInstance(getContext())
                                        .ShowMessage("Informacion",res.getMessage(),transaction);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                pb_createIncome.setVisibility(View.INVISIBLE);
                                if(error.networkResponse!=null){
                                    try {
                                        String s=new String(error.networkResponse.data,
                                                HttpHeaderParser.parseCharset(error.networkResponse.headers));
                                        com.app.expensesapp.Models.Response _Response=gson
                                                .fromJson(s, com.app.expensesapp.Models.Response.class);
                                        AlertMessage.getInstance(getContext())
                                                .ShowMessage("Informacion",_Response.getMessage());
                                    } catch (UnsupportedEncodingException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                                else {
                                    AlertMessage.getInstance(getContext())
                                            .ShowMessage("Informacion",error.toString());
                                }
                            }
                        });

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                VolleySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
            }
        });
    }

    @Override
    public boolean ValidateFields() {
        if(!txt_incomeAmout.getText().toString().equals("") &&
            !txt_dateIncome.getText().toString().equals(""))return true;
        else return false;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
