package com.app.expensesapp.Util;

public abstract class Components {
    public abstract void ClearFields();
    public abstract boolean Validate();
}
