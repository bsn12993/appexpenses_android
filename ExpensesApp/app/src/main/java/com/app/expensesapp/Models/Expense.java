package com.app.expensesapp.Models;

import com.app.expensesapp.Util.Components;

import java.util.Date;

public class Expense extends Components {

    private int Expense_Id;
    private String Date;
    private double Amount;
    private int Category_Id;
    private int User_Id;
    private Category Category;
    private User User;

    public Expense() {
    }

    public int getExpense_Id() {
        return Expense_Id;
    }

    public void setExpense_Id(int expense_Id) {
        Expense_Id = expense_Id;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double amount) {
        Amount = amount;
    }

    public int getCategory_Id() {
        return Category_Id;
    }

    public void setCategory_Id(int category_Id) {
        Category_Id = category_Id;
    }

    public int getUser_Id() {
        return User_Id;
    }

    public void setUser_Id(int user_Id) {
        User_Id = user_Id;
    }

    public com.app.expensesapp.Models.Category getCategory() {
        return Category;
    }

    public void setCategory(com.app.expensesapp.Models.Category category) {
        Category = category;
    }

    public com.app.expensesapp.Models.User getUser() {
        return User;
    }

    public void setUser(com.app.expensesapp.Models.User user) {
        User = user;
    }

    @Override
    public void ClearFields() {
        setUser_Id(0);
        setAmount(0);
        setDate("");

    }

    @Override
    public boolean Validate() {
        if(getAmount()!=0 &&
        getCategory_Id()!=0 &&
        !getDate().equals(""))return true;
        else return false;
    }

    public boolean IsValidAmount(){
        return (getAmount()!=0)?true:false;
    }

    public boolean IsValidCategory(){
        return (getCategory_Id()!=0)?true:false;
    }

    public boolean IsValidDate(){
        return (!getDate().equals(""))?true:false;
    }
}
