package com.app.expensesapp.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.expensesapp.Interface.IApiService;
import com.app.expensesapp.Interface.IFields;
import com.app.expensesapp.Models.Response;
import com.app.expensesapp.Models.User;
import com.app.expensesapp.R;
import com.app.expensesapp.SQLite.SQLiteConnection;
import com.app.expensesapp.SQLite.Schema;
import com.app.expensesapp.Util.AlertMessage;
import com.app.expensesapp.Util.Config;
import com.app.expensesapp.Util.VolleySingleton;
import com.google.gson.Gson;

import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProfileFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment implements
        IFields
{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Components
     */
    private TextView txtId;
    private TextView txtName;
    private TextView txtEmail;
    private TextView txt_userLastNameProfile;
    //private ProgressBar pb_profile;
    //private FloatingActionButton fab_editUser;
    private TextView txt_userPasswordProfile;
    private Button btn_editName;
    private Button btn_editLastName;
    private Button btn_editEmail;
    private Button btn_editPassword;

    /**
     * Fragments
     */
    private FragmentTransaction transaction;
    private UserFragment userFragment;

    /**
     * Objects
     */
    private  Bundle bundle;
    private SQLiteConnection connection;
    private JsonObjectRequest jsonObjectRequest;
    private Gson gson;

    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_profile, container, false);
        InitComponents(view);
        ButtonEvents();
        userFragment=new UserFragment();
        gson=new Gson();
        view.setFitsSystemWindows(true);
        return  view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void Get(String url, Context context) {
        jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url, null,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Response res=gson.fromJson(response.toString(),Response.class);
                        String json=gson.toJson(res.getResult());
                        User user=gson.fromJson(json,User.class);
                        txtName.setText(user.getName());
                        txt_userLastNameProfile.setText(user.getFullName());
                        txtEmail.setText(user.getEmail());
                        txt_userPasswordProfile.setText(user.getPassword());
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //pb_profile.setVisibility(View.INVISIBLE);
                        AlertMessage.getInstance(getContext()).ShowMessage("Informacion",error.toString());
                    }
                });
        VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    public void CleanFields() {

    }

    @Override
    public void InitComponents() {

    }

    @Override
    public void InitComponents(View view) {
        txtName=(TextView)view.findViewById(R.id.txt_userNameProfile);
        txtEmail=(TextView)view.findViewById(R.id.txt_userEmailProfile);
        txt_userLastNameProfile=(TextView)view.findViewById(R.id.txt_userLastNameProfile);
        //pb_profile=(ProgressBar)view.findViewById(R.id.pb_profile);
        //fab_editUser=(FloatingActionButton)view.findViewById(R.id.fab_editUser);
        txt_userPasswordProfile=(TextView)view.findViewById(R.id.txt_userPasswordProfile);
        btn_editName=(Button)view.findViewById(R.id.btn_editName);
        btn_editLastName=(Button)view.findViewById(R.id.btn_editLastName);
        btn_editEmail=(Button)view.findViewById(R.id.btn_editEmail);
        btn_editPassword=(Button)view.findViewById(R.id.btn_editPassword);
    }

    @Override
    public void ButtonEvents() {
        connection=new SQLiteConnection(getContext(), Schema.Database_Name,null,1);
        User user=connection.SelectUser(connection);
        txtName.setText(user.getName());
        txt_userLastNameProfile.setText(user.getLastName());
        txtEmail.setText(user.getEmail());
        txt_userPasswordProfile.setText(user.getPassword());

        btn_editName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle=new Bundle();
                bundle.putString("key",Config.getInstance(getContext()).getTextFieldName());
                bundle.putString("val",txtName.getText().toString());
                userFragment.setArguments(bundle);
                transaction=getFragmentManager().beginTransaction();
                transaction.replace(R.id.frameContainer,userFragment).addToBackStack(null).commit();
            }
        });

        btn_editEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle=new Bundle();
                bundle.putString("key",Config.getInstance(getContext()).getTextFieldEmail());
                bundle.putString("val",txtEmail.getText().toString());
                userFragment.setArguments(bundle);
                transaction=getFragmentManager().beginTransaction();
                transaction.replace(R.id.frameContainer,userFragment).addToBackStack(null).commit();
            }
        });

        btn_editLastName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle=new Bundle();
                bundle.putString("key",Config.getInstance(getContext()).getTextFieldLastName());
                bundle.putString("val",txt_userLastNameProfile.getText().toString());
                userFragment.setArguments(bundle);
                transaction=getFragmentManager().beginTransaction();
                transaction.replace(R.id.frameContainer,userFragment).addToBackStack(null).commit();
            }
        });

        btn_editPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle=new Bundle();
                bundle.putString("key",Config.getInstance(getContext()).getTextFieldPassword());
                bundle.putString("val",txt_userPasswordProfile.getText().toString());
                userFragment.setArguments(bundle);
                transaction=getFragmentManager().beginTransaction();
                transaction.replace(R.id.frameContainer,userFragment).addToBackStack(null).commit();
            }
        });
    }

    @Override
    public boolean ValidateFields() {
        return false;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
