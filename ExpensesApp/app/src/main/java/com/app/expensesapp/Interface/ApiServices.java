package com.app.expensesapp.Interface;

import android.content.Context;

import com.app.expensesapp.Models.Response;

public interface ApiServices<T> {

    Response Get(String url);
    Response GetById(String url);
    Response Post(String url, T model);
    Response Put(String url, T model);
    Response Delete(String url);
}
