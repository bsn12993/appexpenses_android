package com.app.expensesapp.SQLite;

import android.content.ContentValues;

import com.app.expensesapp.Models.User;

public class SQLiteManager {

    private static ContentValues contentValues;

    public static ContentValues setContentValues(User user){
        contentValues=new ContentValues();
        contentValues.put(Schema.ID_USER,user.getUser_Id());
        contentValues.put(Schema.NAME,user.getName());
        contentValues.put(Schema.LASTNAME,user.getLastName());
        contentValues.put(Schema.EMAIL,user.getEmail());
        contentValues.put(Schema.PASSWORD,user.getPassword());
        return contentValues;
    }

}
