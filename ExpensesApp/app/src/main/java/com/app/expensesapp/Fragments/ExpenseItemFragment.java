package com.app.expensesapp.Fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.app.expensesapp.Adapters.ExpensesAdapter;
import com.app.expensesapp.Interface.IApiService;
import com.app.expensesapp.Interface.IFields;
import com.app.expensesapp.Models.Category;
import com.app.expensesapp.Models.Expense;
import com.app.expensesapp.Models.User;
import com.app.expensesapp.Models.UserCategory;
import com.app.expensesapp.R;
import com.app.expensesapp.SQLite.SQLiteConnection;
import com.app.expensesapp.SQLite.Schema;
import com.app.expensesapp.Util.AlertMessage;
import com.app.expensesapp.Util.Config;
import com.app.expensesapp.Util.Connectivity;
import com.app.expensesapp.Util.VolleySingleton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ExpenseItemFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ExpenseItemFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ExpenseItemFragment extends Fragment implements
        IFields {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Components
     */
    private Button btn_selectDate;
    private EditText txt_dateExpense;
    private EditText txt_expenseAmount;
    private Button btn_createExpense;
    private Button btn_deleteExpense;
    private FloatingActionButton fab_reloadExpensesItem;
    private Spinner cmb_Categories;
    private ProgressBar pb_createExpense;

    /**
     *
     */
    private int day;
    private int month;
    private int year;
    private String url;
    private ArrayAdapter<String> adapter;

    /**
     * Objects
     */
    private Calendar calendar;
    private Expense expense;
    private Gson gson;
    private SQLiteConnection connection;
    private Bundle bundle;
    private NetworkInfo networkInfo;
    /**
     * Fragments
     */
    private ExpensesFragment expensesFragment;
    private FragmentTransaction transaction;

    public ExpenseItemFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ExpenseItemFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ExpenseItemFragment newInstance(String param1, String param2) {
        ExpenseItemFragment fragment = new ExpenseItemFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_expense_item, container, false);
        expense=new Expense();
        gson=new Gson();
        InitComponents(view);
        ButtonEvents();

        connection=new SQLiteConnection(getContext(), Schema.Database_Name,null,1);
        int idUser=connection.SelectIDUser(connection);
        url=Config.getInstance(getContext()).getUrlWSUserById();
        url=url.replace("{id}",idUser+"");

        Get(url,getContext());

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public List<String> getCategories(List<UserCategory> userCategories){
        List<String> categories=new ArrayList<>();
        categories.add("0 - Selecciona una categoria");
        if(userCategories.size()==0){
            categories.add("0 - No se encontraron datos");
            return categories;
        }
        for (UserCategory userCategory : userCategories){
            categories.add(userCategory.getCategory_Id()+" - "+userCategory.getCategory().getName());
        }

        return categories;
    }

    public void setDataItemSelected(){
        bundle=getArguments();
        if(bundle!=null){
            String date=bundle.getString("date");
            if(date.contains("T"))date=date.split("T")[0];
            String amount=bundle.getString("amount");
            String category_id=bundle.getString("category_id");
            String expense_id=bundle.getString("expense_id");
            String category=bundle.getString("category");
            txt_dateExpense.setText(date);
            txt_expenseAmount.setText(amount);
            int position=adapter.getPosition(category_id+" - "+category);
            cmb_Categories.setSelection(position);
            expense.setExpense_Id(Integer.parseInt(expense_id));
            expense.setDate(date);
            btn_deleteExpense.setVisibility(View.VISIBLE);
        }
    }

    public void Get(String url, Context context) {
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Gson gson=new Gson();
                        com.app.expensesapp.Models.Response res=gson.fromJson(response.toString(), com.app.expensesapp.Models.Response.class);
                        String json=gson.toJson(res.getResult());
                        Type categoryListType=new TypeToken<ArrayList<UserCategory>>(){}.getType();
                        List<UserCategory> userCategory=gson.fromJson(json,categoryListType);
                        List<String> categories=getCategories(userCategory);
                        adapter= new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1,categories);
                        cmb_Categories.setAdapter(adapter);
                        setDataItemSelected();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AlertMessage.getInstance(getContext()).ShowMessage("Informacion",error.toString());
                    }
                });
        VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
    }

    public void Post(String url) {
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pb_createExpense.setVisibility(View.INVISIBLE);
                        com.app.expensesapp.Models.Response res=gson.fromJson(response, com.app.expensesapp.Models.Response.class);
                        expensesFragment=new ExpensesFragment();
                        transaction=getFragmentManager().beginTransaction()
                                .replace(R.id.frameContainer,expensesFragment);
                        AlertMessage.getInstance(getContext()).ShowMessage("Informacion",res.getMessage(),transaction);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pb_createExpense.setVisibility(View.INVISIBLE);
                        if(error.networkResponse!=null){
                            try {
                                String s=new String(error.networkResponse.data,
                                        HttpHeaderParser.parseCharset(error.networkResponse.headers));
                                com.app.expensesapp.Models.Response _Response=gson
                                        .fromJson(s, com.app.expensesapp.Models.Response.class);
                                AlertMessage.getInstance(getContext())
                                        .ShowMessage("Informacion",_Response.getMessage());
                            } catch (UnsupportedEncodingException e1) {
                                e1.printStackTrace();
                            }
                        }
                        else{
                            AlertMessage.getInstance(getContext())
                                    .ShowMessage("Informacion",error.toString());
                        }
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map=new HashMap<>();
                map.put("Date",expense.getDate());
                map.put("Amount",String.valueOf(expense.getAmount()));
                map.put("Category_Id",String.valueOf(expense.getCategory_Id()));
                map.put("User_Id",String.valueOf(expense.getUser_Id()));
                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
    }

    public void Put(String url) {
        StringRequest stringRequest=new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pb_createExpense.setVisibility(View.INVISIBLE);
                        com.app.expensesapp.Models.Response res=gson.fromJson(response, com.app.expensesapp.Models.Response.class);
                        expensesFragment=new ExpensesFragment();
                        transaction=getFragmentManager().beginTransaction()
                                .replace(R.id.frameContainer,expensesFragment);
                        AlertMessage.getInstance(getContext()).ShowMessage("Informacion",res.getMessage(),transaction);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pb_createExpense.setVisibility(View.INVISIBLE);
                        if(error.networkResponse!=null){
                            try {
                                String s=new String(error.networkResponse.data,
                                        HttpHeaderParser.parseCharset(error.networkResponse.headers));
                                com.app.expensesapp.Models.Response _Response=gson
                                        .fromJson(s, com.app.expensesapp.Models.Response.class);
                                AlertMessage.getInstance(getContext())
                                        .ShowMessage("Informacion",_Response.getMessage());
                            } catch (UnsupportedEncodingException e1) {
                                e1.printStackTrace();
                            }
                        }
                        else {
                            AlertMessage.getInstance(getContext())
                                    .ShowMessage("Informacion",error.toString());
                        }
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map=new HashMap<>();
                map.put("Date",expense.getDate());
                map.put("Amount",String.valueOf(expense.getAmount()));
                map.put("Category_Id",String.valueOf(expense.getCategory_Id()));
                map.put("User_Id",String.valueOf(expense.getUser_Id()));
                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
    }

    @Override
    public void CleanFields() {

    }

    @Override
    public void InitComponents() {

    }

    @Override
    public void InitComponents(View view) {
        pb_createExpense=view.findViewById(R.id.pb_createExpense);
        txt_dateExpense=view.findViewById(R.id.txt_dateExpense);
        txt_expenseAmount=view.findViewById(R.id.txt_expenseAmount);
        btn_selectDate=view.findViewById(R.id.btn_selectDateExpense);
        btn_createExpense=view.findViewById(R.id.btn_createExpense);
        fab_reloadExpensesItem=view.findViewById(R.id.fab_reloadExpensesItem);
        cmb_Categories=view.findViewById(R.id.cmb_Categories);
        btn_deleteExpense=view.findViewById(R.id.btn_deleteExpense);
    }

    @Override
    public void ButtonEvents() {
        btn_selectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar=Calendar.getInstance();
                day=calendar.get(Calendar.DAY_OF_MONTH);
                month=calendar.get(Calendar.MONTH);
                year=calendar.get(Calendar.YEAR);

                DatePickerDialog pickerDialog=new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        txt_dateExpense.setText(dayOfMonth+"/"+(month+1)+"/"+year);
                        expense.setDate(year+"/"+(month+1)+"/"+dayOfMonth);
                    }
                },year,month,day);
                pickerDialog.show();
            }
        });


        btn_createExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ValidateFields()){
                    Toast.makeText(getContext(),"Campos vacios",Toast.LENGTH_LONG).show();
                    return;
                }
                SQLiteConnection connection=new SQLiteConnection(getContext(), Schema.Database_Name,null,1);
                int iduser=connection.SelectIDUser(connection);
                expense.setAmount(Double.parseDouble(txt_expenseAmount.getText().toString()));
                expense.setUser_Id(iduser);

                networkInfo= Connectivity.getInstance(getContext()).CheckInternetConnection();
                if(networkInfo==null || !networkInfo.isConnected()){
                    AlertMessage.getInstance(getContext())
                            .ShowMessage("Informacion",Config.getInstance(getContext())
                                    .getMessageNoInternetConnection());
                    return;
                }

                pb_createExpense.setVisibility(View.VISIBLE);
                if(expense.getExpense_Id()==0)
                    Post(Config.getInstance(getContext()).getUrlWsCreateExpenseByUser());
                else
                {
                    String url=Config.getInstance(getContext()).getUrlWsUpdateExpense();
                    url=url.replace("{id}",expense.getExpense_Id()+"");
                    Put(url);
                }
            }
        });

        fab_reloadExpensesItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Get(Config.getInstance(getContext()).getEndPoint()+url,getContext());
            }
        });

        cmb_Categories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String categorySelected=parent.getItemAtPosition(position).toString();
                int idCategory=Integer.parseInt(categorySelected.split("-")[0].trim());
                expense.setCategory_Id(idCategory);
                Toast.makeText(getContext(),"Se ha seleccionado "+categorySelected.split("-")[1],Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        btn_deleteExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                networkInfo= Connectivity.getInstance(getContext()).CheckInternetConnection();
                if(networkInfo==null || !networkInfo.isConnected()){
                    AlertMessage.getInstance(getContext())
                            .ShowMessage("Informacion",Config.getInstance(getContext())
                                    .getMessageNoInternetConnection());
                    return;
                }
                pb_createExpense.setVisibility(View.VISIBLE);
                String url= Config.getInstance(getContext()).getUrlWsDeleteExpense();
                url=url.replace("{id}",expense.getExpense_Id()+"");
                StringRequest stringRequest=new StringRequest(Request.Method.DELETE, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                pb_createExpense.setVisibility(View.INVISIBLE);
                                com.app.expensesapp.Models.Response res=gson.fromJson(response,
                                        com.app.expensesapp.Models.Response.class);
                                expensesFragment=new ExpensesFragment();
                                transaction=getFragmentManager().beginTransaction()
                                        .replace(R.id.frameContainer,expensesFragment);
                                AlertMessage.getInstance(getContext())
                                        .ShowMessage("Informacion",res.getMessage(),transaction);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                pb_createExpense.setVisibility(View.INVISIBLE);
                                if (error.networkResponse!=null){
                                    try {
                                        String s=new String(error.networkResponse.data,
                                                HttpHeaderParser.parseCharset(error.networkResponse.headers));
                                        com.app.expensesapp.Models.Response _Response=gson
                                                .fromJson(s, com.app.expensesapp.Models.Response.class);
                                        AlertMessage.getInstance(getContext())
                                                .ShowMessage("Informacion",_Response.getMessage());
                                    } catch (UnsupportedEncodingException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                                else{
                                    AlertMessage.getInstance(getContext())
                                            .ShowMessage("Error",error.toString());
                                }
                            }
                        });
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                VolleySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
            }
        });
    }

    @Override
    public boolean ValidateFields() {
        if(!txt_expenseAmount.getText().toString().equals("") &&
            !txt_dateExpense.getText().toString().equals(""))return true;
        else return false;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
