package com.app.expensesapp.Implement;

import com.android.volley.toolbox.JsonObjectRequest;
import com.app.expensesapp.Models.Response;
import com.google.gson.Gson;

public class VolleyData {

    public JsonObjectRequest jsonObjectRequest;
    public JsonObjectRequest postRequest;
    public Gson gson;
    public Response responseData;

}
