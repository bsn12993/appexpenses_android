package com.app.expensesapp.Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.expensesapp.Adapters.IncomesAdapter;
import com.app.expensesapp.Interface.IFields;
import com.app.expensesapp.SQLite.SQLiteConnection;
import com.app.expensesapp.SQLite.Schema;
import com.app.expensesapp.Util.Config;
import com.app.expensesapp.Interface.IApiService;
import com.app.expensesapp.Models.Income;
import com.app.expensesapp.Models.Response;
import com.app.expensesapp.R;
import com.app.expensesapp.Util.AlertMessage;
import com.app.expensesapp.Util.Connectivity;
import com.app.expensesapp.Util.VolleySingleton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link IncomesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link IncomesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class IncomesFragment extends Fragment implements
        IFields {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Components
     */
    private RecyclerView rvIncomes;
    private ProgressBar pb_incomes;
    private FloatingActionButton fabIncomeItem;
    private TextView txt_noRecordIncome;
    private FloatingActionButton fab_reloadIncomes;
    private ImageView img_noInternetConnectionIncome;

    /**
     * Objects
     */
    private JsonObjectRequest jsonObjectRequest;
    private Gson gson;
    private List<Income> incomeList;
    private NetworkInfo networkInfo;
    private SQLiteConnection connection;

    private IncomeItemFragment itemFragment;
    private FragmentTransaction transaction;

    public IncomesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment IncomesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static IncomesFragment newInstance(String param1, String param2) {
        IncomesFragment fragment = new IncomesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_incomes, container, false);
        InitComponents(view);
        ButtonEvents();
        gson=new Gson();

        networkInfo= Connectivity.getInstance(getContext()).CheckInternetConnection();
        if(networkInfo==null || !networkInfo.isConnected()){
            pb_incomes.setVisibility(View.INVISIBLE);
            img_noInternetConnectionIncome.setVisibility(View.INVISIBLE);
            Toast.makeText(getContext(),Config.getInstance(getContext()).getMessageNoInternetConnection(),Toast.LENGTH_LONG).show();
        }
        else {
            pb_incomes.setVisibility(View.VISIBLE);
            connection=new SQLiteConnection(getContext(), Schema.Database_Name,null,1);
            int idUser=connection.SelectIDUser(connection);
            String url=Config.getInstance(getContext()).getUrlWSIncomesByIdUser();
            url=url.replace("{iduser}",idUser+"");
            Get(url,getContext());
        }
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void Get(String url, Context context) {
        jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url, null,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        pb_incomes.setVisibility(View.INVISIBLE);
                        txt_noRecordIncome.setText("");
                        Response res=gson.fromJson(response.toString(),Response.class);
                        String json=gson.toJson(res.getResult());
                        Type incomesTypeList=new TypeToken<ArrayList<Income>>(){}.getType();
                        incomeList=gson.fromJson(json,incomesTypeList);
                        itemFragment=new IncomeItemFragment();
                        transaction=getFragmentManager().beginTransaction();
                        IncomesAdapter incomesAdapter=new IncomesAdapter(incomeList,transaction,itemFragment,getContext());
                        rvIncomes.setAdapter(incomesAdapter);
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pb_incomes.setVisibility(View.INVISIBLE);
                        if (error.networkResponse!=null){
                            try {
                                String s=new String(error.networkResponse.data,
                                        HttpHeaderParser.parseCharset(error.networkResponse.headers));
                                com.app.expensesapp.Models.Response _Response=gson.fromJson(s, com.app.expensesapp.Models.Response.class);
                                AlertMessage.getInstance(getContext())
                                        .ShowMessage("Informacion",_Response.getMessage());
                            } catch (UnsupportedEncodingException e1) {
                                e1.printStackTrace();
                            }
                        }
                        else {
                            txt_noRecordIncome.setText(Config.getInstance(getContext()).getMessageInfo());
                            Toast.makeText(getContext(),error.toString(),Toast.LENGTH_LONG).show();
                        }
                    }
                });
        VolleySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    public void CleanFields() {

    }

    @Override
    public void InitComponents() {

    }

    @Override
    public void InitComponents(View view) {
        rvIncomes=(RecyclerView)view.findViewById(R.id.rvIncomes);
        rvIncomes.setLayoutManager(new LinearLayoutManager(getContext()));
        pb_incomes=(ProgressBar)view.findViewById(R.id.pb_incomes);
        txt_noRecordIncome=(TextView)view.findViewById(R.id.txt_noRecordIncome);
        img_noInternetConnectionIncome=(ImageView)view.findViewById(R.id.img_noInternetConnectionIncome);
        fabIncomeItem=(FloatingActionButton)view.findViewById(R.id.fab_incomeItem);
        fab_reloadIncomes=(FloatingActionButton)view.findViewById(R.id.fab_reloadIncomes);
    }

    @Override
    public void ButtonEvents() {
        fabIncomeItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemFragment=new IncomeItemFragment();
                FragmentTransaction transaction=getFragmentManager().beginTransaction();
                transaction.replace(R.id.frameContainer,itemFragment).commit();
            }
        });


        fab_reloadIncomes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_noRecordIncome.setText("");
                img_noInternetConnectionIncome.setVisibility(View.INVISIBLE);
                networkInfo= Connectivity.getInstance(getContext()).CheckInternetConnection();
                if(networkInfo==null || !networkInfo.isConnected()){
                    img_noInternetConnectionIncome.setVisibility(View.VISIBLE);
                    Toast.makeText(getContext(),Config.getInstance(getContext()).getMessageNoInternetConnection(),Toast.LENGTH_LONG).show();
                }
                else {
                    pb_incomes.setVisibility(View.VISIBLE);
                    connection=new SQLiteConnection(getContext(), Schema.Database_Name,null,1);
                    int idUser=connection.SelectIDUser(connection);
                    String url=Config.getInstance(getContext()).getUrlWSIncomesByIdUser();
                    url=url.replace("{iduser}",idUser+"");
                    Get(url,getContext());
                }
            }
        });
    }

    @Override
    public boolean ValidateFields() {
        return false;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
