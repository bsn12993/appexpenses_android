package com.app.expensesapp.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.app.expensesapp.Models.User;

public class SQLiteConnection extends SQLiteOpenHelper {

    private SQLiteDatabase database;

    public SQLiteConnection(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Schema.getCreateTable());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(Schema.getDropTable());
        onCreate(db);
    }

    public long Insert(SQLiteConnection connection, User user){
        database=connection.getWritableDatabase();
        long idRecord=database.insert(Schema.Table_Name,null,SQLiteManager.setContentValues(user));
        database.close();
        return idRecord;
    }

    public User SelectUser(SQLiteConnection connection){
        User user=new User();
        database=connection.getReadableDatabase();
        try {
            //Cursor cursor=database.query(Schema.Table_Name,fields,"*",null,null,null,null);
            Cursor cursor=database.rawQuery("select * from "+Schema.Table_Name,null);
            if(cursor.getCount()>0){
                cursor.moveToFirst();
                user.setUser_Id(cursor.getInt(1));
                user.setName(cursor.getString(2));
                user.setLastName(cursor.getString(3));
                user.setEmail(cursor.getString(4));
                user.setPassword(cursor.getString(5));
            }
            else return null;
            cursor.close();
            return user;
        }catch (Exception e){
            return null;
        }
        finally {
            database.close();
        }
    }

    public int SelectIDUser(SQLiteConnection connection){
        database=connection.getReadableDatabase();
        String[] fields={Schema.ID,Schema.NAME,Schema.LASTNAME,Schema.EMAIL};
        int idUser=0;
        try {
            Cursor cursor=database.rawQuery("select "+Schema.ID_USER+" from "+Schema.Table_Name,null);
            if(cursor.getCount()>0){
                cursor.moveToFirst();
                idUser=cursor.getInt(0);
            }
        }catch (Exception e){
            return 0;
        }
        finally {
            database.close();
        }
        return idUser;
    }

    public boolean UpdateUser(SQLiteConnection connection, User user){
        database=connection.getWritableDatabase();
        String[] params={String.valueOf(user.getUser_Id())};
        int record=0;
        ContentValues values=new ContentValues();
        values.put(Schema.ID_USER,user.getUser_Id());
        values.put(Schema.NAME,user.getName());
        values.put(Schema.LASTNAME,user.getLastName());
        values.put(Schema.EMAIL,user.getEmail());
        values.put(Schema.PASSWORD,user.getPassword());
        record=database.update(Schema.Table_Name,values,Schema.ID_USER+"=?",params);
        database.close();
        return (record>0)?true:false;
    }

    public boolean UpdateUserName(SQLiteConnection connection, User user){
        database=connection.getWritableDatabase();
        String[] params={String.valueOf(user.getUser_Id())};
        int record=0;
        ContentValues values=new ContentValues();
        values.put(Schema.ID_USER,user.getUser_Id());
        values.put(Schema.NAME,user.getName());
        record=database.update(Schema.Table_Name,values,Schema.ID_USER+"=?",params);
        database.close();
        return (record>0)?true:false;
    }

    public boolean UpdateUserLastName(SQLiteConnection connection, User user){
        database=connection.getWritableDatabase();
        String[] params={String.valueOf(user.getUser_Id())};
        int record=0;
        ContentValues values=new ContentValues();
        values.put(Schema.ID_USER,user.getUser_Id());
        values.put(Schema.LASTNAME,user.getLastName());
        record=database.update(Schema.Table_Name,values,Schema.ID_USER+"=?",params);
        database.close();
        return (record>0)?true:false;
    }

    public boolean UpdateUserEmail(SQLiteConnection connection, User user){
        database=connection.getWritableDatabase();
        String[] params={String.valueOf(user.getUser_Id())};
        int record=0;
        ContentValues values=new ContentValues();
        values.put(Schema.ID_USER,user.getUser_Id());
        values.put(Schema.EMAIL,user.getEmail());
        record=database.update(Schema.Table_Name,values,Schema.ID_USER+"=?",params);
        database.close();
        return (record>0)?true:false;
    }


    public boolean UpdateUserPassword(SQLiteConnection connection, User user){
        database=connection.getWritableDatabase();
        String[] params={String.valueOf(user.getUser_Id())};
        int record=0;
        ContentValues values=new ContentValues();
        values.put(Schema.ID_USER,user.getUser_Id());
        values.put(Schema.PASSWORD,user.getPassword());
        record=database.update(Schema.Table_Name,values,Schema.ID_USER+"=?",params);
        database.close();
        return (record>0)?true:false;
    }


    public boolean DeleteUser(SQLiteConnection connection, int iduser){
        database=connection.getWritableDatabase();
        String[] params={ Schema.ID_USER};
        Cursor cursor=database.rawQuery("delete from "+Schema.Table_Name,null);
        int record=cursor.getCount();
        database.close();
        return (record>0)?true:false;
    }

}
