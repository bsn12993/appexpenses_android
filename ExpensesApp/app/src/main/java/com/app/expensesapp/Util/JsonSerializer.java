package com.app.expensesapp.Util;

import com.app.expensesapp.Models.Response;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class JsonSerializer<T> {
    Gson gson=new Gson();

    public List<T> Deserializer(Response response){
        List<T> List=new ArrayList<>();
        String json=gson.toJson(response.getResult());
        Type categoryListType=new TypeToken<ArrayList<T>>(){}.getType();
        List= gson.fromJson(json,categoryListType);
        return List;
    }

    public <T>String ConvertToJSON(T model){
        return gson.toJson(model);
    }


}
