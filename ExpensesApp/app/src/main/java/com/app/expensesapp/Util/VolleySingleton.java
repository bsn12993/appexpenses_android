package com.app.expensesapp.Util;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class VolleySingleton {

    private static VolleySingleton instance;
    private RequestQueue queue;
    private static Context context;
    
    private VolleySingleton(Context ctx){
        context=ctx;
        queue=getRequestQueue();
    }

    private RequestQueue getRequestQueue() {
        if(queue==null){
            queue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return queue;
    }

    public static synchronized VolleySingleton getInstance(Context context){
        if(instance==null)return new VolleySingleton(context);
        else return instance;
    }

    public <T> void addToRequestQueue(Request<T> request){
        getRequestQueue().add(request);
    }
}
