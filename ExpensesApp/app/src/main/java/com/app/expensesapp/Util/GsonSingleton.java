package com.app.expensesapp.Util;

import com.google.gson.Gson;

public class GsonSingleton {
    private Gson gson;
    private static GsonSingleton instance;

    private GsonSingleton(){
        gson=getGson();
    }

    private Gson getGson() {
        if(gson==null)return new Gson();
        else return gson;
    }

    public static GsonSingleton getInstance(){
        if(instance==null)return new GsonSingleton();
        else return instance;
    }
}
