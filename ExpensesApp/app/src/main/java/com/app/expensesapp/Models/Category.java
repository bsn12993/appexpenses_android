package com.app.expensesapp.Models;

import com.app.expensesapp.Util.Components;

public class Category extends Components {

    private int Category_Id;
    private String Name;
    private String Description;
    private int Status;

    public Category() {
    }

    public Category(int category_Id, String name, String description, int status) {
        Category_Id = category_Id;
        Name = name;
        Description = description;
        Status = status;
    }

    public int getCategory_Id() {
        return Category_Id;
    }

    public void setCategory_Id(int category_Id) {
        Category_Id = category_Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    @Override
    public void ClearFields() {

    }

    @Override
    public boolean Validate() {
        if(!getName().equals("") &&
            !getDescription().equals(""))return true;
        else return false;
    }

    public boolean IsValidName(){
        return (!getName().equals(""))?true:false;
    }

    public boolean IsValidDescription(){
        return (!getDescription().equals(""))?true:false;
    }
}
