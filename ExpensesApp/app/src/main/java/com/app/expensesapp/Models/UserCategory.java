package com.app.expensesapp.Models;

public class UserCategory {

    private int UserCategory_Id;
    private int User_Id;
    private User User;
    private int Category_Id;
    private Category Category;

    public UserCategory() {
    }

    public int getUserCategory_Id() {
        return UserCategory_Id;
    }

    public void setUserCategory_Id(int userCategory_Id) {
        UserCategory_Id = userCategory_Id;
    }

    public int getUser_Id() {
        return User_Id;
    }

    public void setUser_Id(int user_Id) {
        User_Id = user_Id;
    }

    public com.app.expensesapp.Models.User getUser() {
        return User;
    }

    public void setUser(com.app.expensesapp.Models.User user) {
        User = user;
    }

    public int getCategory_Id() {
        return Category_Id;
    }

    public void setCategory_Id(int category_Id) {
        Category_Id = category_Id;
    }

    public com.app.expensesapp.Models.Category getCategory() {
        return Category;
    }

    public void setCategory(com.app.expensesapp.Models.Category category) {
        Category = category;
    }
}
