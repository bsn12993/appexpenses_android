package com.app.expensesapp.Models;

import com.app.expensesapp.Util.Components;
import com.app.expensesapp.Util.Util;

public class User extends Components {
    private int User_Id;
    private String Name;
    private String LastName;
    private String Email;
    private String Password;

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public User() {
    }

    public User(int user_Id, String name, String lastName, String email) {
        User_Id = user_Id;
        Name = name;
        LastName = lastName;
        Email = email;
    }

    public int getUser_Id() {
        return User_Id;
    }

    public void setUser_Id(int user_Id) {
        User_Id = user_Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getFullName(){
        return getName()+" "+getLastName();
    }

    public boolean validatePassword(){
        return (!getPassword().equals(""))?true:false;
    }

    public boolean validateEmail(){
        return Util.ValidateEmail(getEmail());
    }

    public boolean validateName(){
        return (!getName().equals(""))?true:false;
    }

    public boolean validateLastName(){
        return (!getLastName().equals(""))?true:false;
    }

    @Override
    public void ClearFields() {
        setName("");
        setLastName("");
        setEmail("");
        setPassword("");
    }

    @Override
    public boolean Validate() {
        if(!getName().equals("") &&
            !getLastName().equals("") &&
            !getPassword().equals("") &&
            validateEmail())return true;
        else return false;
    }
}
