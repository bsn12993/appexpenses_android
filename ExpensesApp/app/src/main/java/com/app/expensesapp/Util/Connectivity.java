package com.app.expensesapp.Util;

import android.content.ContentProviderClient;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Connectivity {

    private ConnectivityManager connectivityManager;
    private Context context;
    private static Connectivity instance;
    private NetworkInfo networkInfo;

    private Connectivity(Context ctx){
        context=ctx;
    }

    public static Connectivity getInstance(Context context){
        if(instance==null)return new Connectivity(context);
        else return instance;
    }

    public NetworkInfo CheckInternetConnection(){
        connectivityManager=getConnectionManager();
        networkInfo=getNetworkInfo(connectivityManager);
        return networkInfo;
    }

    private NetworkInfo getNetworkInfo(ConnectivityManager connectivityManager) {
        if(networkInfo==null)return connectivityManager.getActiveNetworkInfo();
        else return networkInfo;
    }

    private ConnectivityManager getConnectionManager() {
        if(connectivityManager==null){
            return (ConnectivityManager)context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        }
        else
            return connectivityManager;
    }


}
