package com.app.expensesapp.Fragments;

import android.content.Context;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.expensesapp.Adapters.CategoryAdapter;
import com.app.expensesapp.Interface.IApiService;
import com.app.expensesapp.Interface.IFields;
import com.app.expensesapp.Models.Category;
import com.app.expensesapp.Models.Response;
import com.app.expensesapp.Models.UserCategory;
import com.app.expensesapp.R;
import com.app.expensesapp.SQLite.SQLiteConnection;
import com.app.expensesapp.SQLite.Schema;
import com.app.expensesapp.Util.AlertMessage;
import com.app.expensesapp.Util.Config;
import com.app.expensesapp.Util.Connectivity;
import com.app.expensesapp.Util.VolleySingleton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CategoryFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CategoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CategoryFragment extends Fragment implements
        IFields {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Components
     */
    private RecyclerView rvCategory;
    private FloatingActionButton fabCategoryItem;
    private TextView txt_noRecordCategory;
    private FloatingActionButton fab_reloadCategory;
    private ImageView img_noInternetConnectionCategory;

    /**
     * Objetos
     */
    private List<Category> categoryList;
    private ProgressBar pb_category;
    private JsonObjectRequest jsonObjectRequest;
    private Gson gson;
    private NetworkInfo networkInfo;
    private SQLiteConnection connection;

    /**
     * Fragments
     */
    private CategoryItemFragment itemFragment;
    private FragmentTransaction transaction;

    public CategoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CategoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CategoryFragment newInstance(String param1, String param2) {
        CategoryFragment fragment = new CategoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_category, container, false);
        gson=new Gson();
        InitComponents(view);
        ButtonEvents();

        networkInfo= Connectivity.getInstance(getContext()).CheckInternetConnection();
        if(networkInfo==null || !networkInfo.isConnected()){
            pb_category.setVisibility(View.INVISIBLE);
            img_noInternetConnectionCategory.setVisibility(View.VISIBLE);
            Toast.makeText(getContext(),Config.getInstance(getContext()).getMessageNoInternetConnection(),Toast.LENGTH_LONG).show();
        }
        else {
            pb_category.setVisibility(View.VISIBLE);
            connection=new SQLiteConnection(getContext(), Schema.Database_Name,null,1);
            int idUser=connection.SelectIDUser(connection);
            String url=Config.getInstance(getContext()).getUrlWSUserById();
            url=url.replace("{id}",idUser+"");
            Get(url, getContext());
        }
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void Get(String url, Context context) {
        jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url, null,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        pb_category.setVisibility(View.INVISIBLE);
                        txt_noRecordCategory.setText("");
                        Response res=gson.fromJson(response.toString(),Response.class);
                        Type categoryListType=new TypeToken<ArrayList<UserCategory>>(){}.getType();
                        List<UserCategory> userCategories=new ArrayList<>();
                        String json=gson.toJson(res.getResult());
                        userCategories= gson.fromJson(json,categoryListType);
                        itemFragment=new CategoryItemFragment();
                        transaction=getFragmentManager().beginTransaction();
                        CategoryAdapter categoryAdapter=new CategoryAdapter(userCategories,getContext(),transaction,itemFragment);
                        rvCategory.setAdapter(categoryAdapter);
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pb_category.setVisibility(View.INVISIBLE);
                        if (error.networkResponse!=null){
                            try {
                                String s=new String(error.networkResponse.data,
                                        HttpHeaderParser.parseCharset(error.networkResponse.headers));
                                Response _Response=gson.fromJson(s,Response.class);
                                AlertMessage.getInstance(getContext())
                                        .ShowMessage("Informacion",_Response.getMessage());
                            } catch (UnsupportedEncodingException e1) {
                                e1.printStackTrace();
                            }
                        }
                        else {
                            AlertMessage.getInstance(getContext())
                                    .ShowMessage("Informacion",error.toString());
                            txt_noRecordCategory.setText(Config.getInstance(getContext()).getMessageInfo());
                        }
                    }
                });
        VolleySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    public void CleanFields() {

    }

    @Override
    public void InitComponents() {

    }

    @Override
    public void InitComponents(View view) {
        pb_category=(ProgressBar)view.findViewById(R.id.pb_category);
        categoryList=new ArrayList<>();
        rvCategory=(RecyclerView)view.findViewById(R.id.rvCategories);
        rvCategory.setLayoutManager(new LinearLayoutManager(getContext()));
        txt_noRecordCategory=(TextView)view.findViewById(R.id.txt_noRecordCategory);
        img_noInternetConnectionCategory=(ImageView)view.findViewById(R.id.img_noInternetConnectionCategory);
        fabCategoryItem=(FloatingActionButton)view.findViewById(R.id.fab_categoryItem);
        fab_reloadCategory=(FloatingActionButton)view.findViewById(R.id.fab_reloadCategory);
    }

    @Override
    public void ButtonEvents() {
        fabCategoryItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemFragment=new CategoryItemFragment();
                FragmentTransaction transaction=getFragmentManager().beginTransaction();
                transaction.replace(R.id.frameContainer,itemFragment);
                transaction.commit();
            }
        });

        fab_reloadCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_noRecordCategory.setText("");
                img_noInternetConnectionCategory.setVisibility(View.INVISIBLE);
                networkInfo=Connectivity.getInstance(getContext()).CheckInternetConnection();
                if(networkInfo==null || !networkInfo.isConnected()){
                    img_noInternetConnectionCategory.setVisibility(View.VISIBLE);
                    Toast.makeText(getContext(),Config.getInstance(getContext()).getMessageNoInternetConnection(),Toast.LENGTH_LONG).show();
                    return;
                }
                connection=new SQLiteConnection(getContext(), Schema.Database_Name,null,1);
                int idUser=connection.SelectIDUser(connection);
                String url=Config.getInstance(getContext()).getUrlWSUserById();
                url=url.replace("{id}",idUser+"");
                pb_category.setVisibility(View.VISIBLE);
                Get(url, getContext());
            }
        });
    }

    @Override
    public boolean ValidateFields() {
        return false;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
