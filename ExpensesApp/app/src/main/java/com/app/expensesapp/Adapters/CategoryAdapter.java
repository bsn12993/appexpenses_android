package com.app.expensesapp.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.app.expensesapp.Models.Category;
import com.app.expensesapp.Models.UserCategory;
import com.app.expensesapp.R;
import com.app.expensesapp.Util.Config;
import com.app.expensesapp.Util.VolleySingleton;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolderCategory> implements View.OnClickListener{

    List<UserCategory> lstCategory=new ArrayList<>();
    View.OnClickListener listener;
    Context context;
    Fragment fragment;
    FragmentTransaction transaction;

    public CategoryAdapter(List<UserCategory> _lstCategory, Context context, FragmentTransaction transaction, Fragment fragment){
        this.lstCategory=_lstCategory;
        this.context=context;
        this.transaction=transaction;
        this.fragment=fragment;
    }

    @NonNull
    @Override
    public ViewHolderCategory onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_category,null,false);
        view.setOnClickListener(this);
        return new ViewHolderCategory(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderCategory viewHolderCategory, int i) {
        viewHolderCategory.setCategory(lstCategory.get(i));
    }

    @Override
    public int getItemCount() {
        return lstCategory.size();
    }

    @Override
    public void onClick(View v) {
        if (listener!=null)listener.onClick(v);
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener=listener;
    }

    public class ViewHolderCategory extends RecyclerView.ViewHolder {

        CardView cvCategory;
        TextView CategoryName;
        TextView CategoryDescription;
        Button btn_editCategory;
        Bundle bundle;

        public ViewHolderCategory(@NonNull View itemView) {
            super(itemView);
            cvCategory=(CardView)itemView.findViewById(R.id.cv_categories);
            CategoryName=(TextView)itemView.findViewById(R.id.txt_categoryName);
            CategoryDescription=(TextView)itemView.findViewById(R.id.txt_categoryDescription);
            btn_editCategory=(Button)itemView.findViewById(R.id.btn_editCategory);

            btn_editCategory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bundle=new Bundle();
                    bundle.putString("category_id",cvCategory.getTag().toString());
                    bundle.putString("name",CategoryName.getText().toString());
                    bundle.putString("description",CategoryDescription.getText().toString());
                    fragment.setArguments(bundle);
                    transaction.replace(R.id.frameContainer,fragment).addToBackStack(null);
                    transaction.commit();
                }
            });


        }

        public void setCategory(UserCategory item) {
            cvCategory.setTag(item.getCategory_Id());
            CategoryName.setText(item.getCategory().getName());
            CategoryDescription.setText(item.getCategory().getDescription());
        }

    }
}
