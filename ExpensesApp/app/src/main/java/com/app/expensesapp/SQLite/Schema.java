package com.app.expensesapp.SQLite;

import android.provider.BaseColumns;

public class Schema implements BaseColumns {

    public static final String Database_Name="Expenses";
    public static final String Table_Name="Users";
    public static final String ID="id";
    public static final String ID_USER="user_id";
    public static final String NAME="name";
    public static final String LASTNAME="lastName";
    public static final String EMAIL="email";
    public static final String PASSWORD="password";

    public static String getCreateTable(){
        return "create table "+Table_Name+" (" +
                ID+" integer primary key,"+
                ID_USER+" integer,"+
                NAME+" text,"+
                LASTNAME+" text,"+
                EMAIL+" text,"+
                PASSWORD+" text"+
                ")";
    }

    public  static String getDropTable(){
        return "drop table if exists "+Table_Name;
    }

}
