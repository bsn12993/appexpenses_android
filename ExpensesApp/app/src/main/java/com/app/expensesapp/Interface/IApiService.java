package com.app.expensesapp.Interface;

import android.content.Context;

import com.app.expensesapp.Models.Response;

public interface IApiService {
    void Get(String url, Context context);
    void Get(String url);
    void GetById(String url);
    <T>void Post(String url, T model);
    <T>void Put(String url, T model);
    void Delete(String url);
}
