package com.app.expensesapp.Fragments;

import android.content.Context;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.app.expensesapp.Interface.IApiService;
import com.app.expensesapp.Interface.IFields;
import com.app.expensesapp.Models.Category;
import com.app.expensesapp.Models.User;
import com.app.expensesapp.Models.UserCategory;
import com.app.expensesapp.R;
import com.app.expensesapp.SQLite.SQLiteConnection;
import com.app.expensesapp.SQLite.Schema;
import com.app.expensesapp.Util.AlertMessage;
import com.app.expensesapp.Util.Config;
import com.app.expensesapp.Util.Connectivity;
import com.app.expensesapp.Util.VolleySingleton;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CategoryItemFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CategoryItemFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CategoryItemFragment extends Fragment implements
        IFields {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Components
     */
    private Button btn_createCategory;
    private EditText txt_categoryName;
    private EditText txt_categoryDescription;
    private ProgressBar pb_categoryItem;
    private Button btn_deleteCategory;

    /**
     * Objects
     */
    private SQLiteConnection connection;
    private Category category;
    private UserCategory userCategory;
    private User user;
    private Gson gson;
    private Bundle bundle;
    private NetworkInfo networkInfo;

    /**
     * Fragments
     */
    private FragmentTransaction transaction;
    private CategoryFragment categoryFragment;

    public CategoryItemFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CategoryItemFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CategoryItemFragment newInstance(String param1, String param2) {
        CategoryItemFragment fragment = new CategoryItemFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_category_item, container, false);
        gson=new Gson();
        category=new Category();
        InitComponents(view);
        ButtonEvents();
        bundle=getArguments();
        if(bundle!=null){
            int id=Integer.parseInt(bundle.getString("category_id"));
            String name=bundle.getString("name");
            String description=bundle.getString("description");
            txt_categoryName.setText(name);
            txt_categoryDescription.setText(description);

            category.setCategory_Id(id);
            category.setName(name);
            category.setDescription(description);
            category.setStatus(1);

            btn_deleteCategory.setVisibility(View.VISIBLE);
        }
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public void Post(String url) {
        JSONObject jsonCategory=new JSONObject();
        JSONObject jsonUserCategory=new JSONObject();

        try {
            jsonCategory.put("Category_Id",userCategory.getCategory().getCategory_Id());
            jsonCategory.put("Name",userCategory.getCategory().getName());
            jsonCategory.put("Description",userCategory.getCategory().getDescription());
            jsonCategory.put("Status",userCategory.getCategory().getStatus());

            jsonUserCategory.put("UserCategory_Id",userCategory.getUserCategory_Id());
            jsonUserCategory.put("User_Id",userCategory.getUser().getUser_Id());
            jsonUserCategory.put("User",null);
            jsonUserCategory.put("Category",jsonCategory);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jobReq = new JsonObjectRequest(Request.Method.POST, url, jsonUserCategory,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        pb_categoryItem.setVisibility(View.INVISIBLE);
                        com.app.expensesapp.Models.Response res=gson.fromJson(jsonObject.toString(),
                                com.app.expensesapp.Models.Response.class);
                        categoryFragment=new CategoryFragment();
                        transaction=getFragmentManager().beginTransaction()
                                .replace(R.id.frameContainer,categoryFragment);
                        AlertMessage.getInstance(getContext())
                                .ShowMessage("Informacion",res.getMessage(),transaction);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        pb_categoryItem.setVisibility(View.INVISIBLE);
                        if(volleyError.networkResponse!=null){
                            try {
                                String s=new String(volleyError.networkResponse.data,
                                        HttpHeaderParser.parseCharset(volleyError.networkResponse.headers));
                                com.app.expensesapp.Models.Response _Response=gson.fromJson(s, com.app.expensesapp.Models.Response.class);
                                AlertMessage.getInstance(getContext())
                                        .ShowMessage("Informacion",_Response.getMessage());
                            } catch (UnsupportedEncodingException e1) {
                                e1.printStackTrace();
                            }
                        }
                        else {
                            AlertMessage.getInstance(getContext())
                                    .ShowMessage("Informacion",volleyError.toString());
                        }
                    }
                });
        VolleySingleton.getInstance(getContext()).addToRequestQueue(jobReq);
    }

    public void Put(String url) {
        JSONObject jsonCategory=new JSONObject();

        try {
            jsonCategory.put("Category_Id",userCategory.getCategory().getCategory_Id());
            jsonCategory.put("Name",userCategory.getCategory().getName());
            jsonCategory.put("Description",userCategory.getCategory().getDescription());
            jsonCategory.put("Status",userCategory.getCategory().getStatus());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jobReq = new JsonObjectRequest(Request.Method.PUT, url, jsonCategory,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        pb_categoryItem.setVisibility(View.INVISIBLE);
                        com.app.expensesapp.Models.Response res=gson.fromJson(jsonObject.toString(),
                                com.app.expensesapp.Models.Response.class);
                        categoryFragment=new CategoryFragment();
                        transaction=getFragmentManager().beginTransaction()
                                .replace(R.id.frameContainer,categoryFragment);
                        AlertMessage.getInstance(getContext())
                                .ShowMessage("Informacion",res.getMessage(),transaction);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        pb_categoryItem.setVisibility(View.INVISIBLE);
                        if(volleyError.networkResponse!=null){
                            try {
                                String s=new String(volleyError.networkResponse.data,
                                        HttpHeaderParser.parseCharset(volleyError.networkResponse.headers));
                                com.app.expensesapp.Models.Response _Response=gson
                                        .fromJson(s, com.app.expensesapp.Models.Response.class);
                                AlertMessage.getInstance(getContext())
                                        .ShowMessage("Informacion",_Response.getMessage());
                            } catch (UnsupportedEncodingException e1) {
                                e1.printStackTrace();
                            }
                        }
                        else {
                            AlertMessage.getInstance(getContext())
                                    .ShowMessage("Informacion",volleyError.toString());
                        }
                    }
                });
        VolleySingleton.getInstance(getContext()).addToRequestQueue(jobReq);
    }


    @Override
    public void CleanFields() {

    }

    @Override
    public void InitComponents() {

    }

    @Override
    public void InitComponents(View view) {
        pb_categoryItem=view.findViewById(R.id.pb_categoryItem);
        txt_categoryName=view.findViewById(R.id.txt_categoryName);
        txt_categoryDescription=view.findViewById(R.id.txt_categoryDescription);
        btn_createCategory=view.findViewById(R.id.btn_createCategory);
        btn_deleteCategory=view.findViewById(R.id.btn_deleteCategory);
    }

    @Override
    public void ButtonEvents() {
        btn_createCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ValidateFields()){
                    Toast.makeText(getContext(),"Campos vacios",Toast.LENGTH_LONG).show();
                    txt_categoryDescription.setFocusable(true);
                    return;
                }
                connection=new SQLiteConnection(getContext(), Schema.Database_Name,null,1);
                user =connection.SelectUser(connection);

                category.setName(txt_categoryName.getText().toString());
                category.setDescription(txt_categoryDescription.getText().toString());
                category.setStatus(1);

                userCategory=new UserCategory();
                userCategory.setCategory(category);
                userCategory.setUser(user);

                networkInfo= Connectivity.getInstance(getContext()).CheckInternetConnection();
                if(networkInfo==null || !networkInfo.isConnected()){
                    AlertMessage.getInstance(getContext())
                            .ShowMessage("Informacion",Config.getInstance(getContext())
                                    .getMessageNoInternetConnection());
                    return;
                 }

                pb_categoryItem.setVisibility(View.VISIBLE);
                if(category.getCategory_Id()==0){
                    Post(Config.getInstance(getContext()).getUrlWsCreateCategoryByUser());
                }
                else {
                    String url=Config.getInstance(getContext()).getUrlWsUpdateCategory();
                    url=url.replace("{id}",category.getCategory_Id()+"");
                    Put(url);
                }
            }
        });

        btn_deleteCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                networkInfo= Connectivity.getInstance(getContext()).CheckInternetConnection();
                if(networkInfo==null || !networkInfo.isConnected()){
                    AlertMessage.getInstance(getContext())
                            .ShowMessage("Informacion",Config.getInstance(getContext())
                                    .getMessageNoInternetConnection());
                    return;
                }
                pb_categoryItem.setVisibility(View.VISIBLE);
                connection=new SQLiteConnection(getContext(), Schema.Database_Name,null,1);
                user =connection.SelectUser(connection);
                String url= Config.getInstance(getContext()).getUrlWsDeleteUserCategory();
                url=url.replace("{id}",category.getCategory_Id()+"");
                url=url.replace("{iduser}",user.getUser_Id()+"");
                StringRequest stringRequest=new StringRequest(Request.Method.DELETE, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                pb_categoryItem.setVisibility(View.INVISIBLE);
                                com.app.expensesapp.Models.Response res=gson.fromJson(response,
                                        com.app.expensesapp.Models.Response.class);
                                categoryFragment=new CategoryFragment();
                                transaction=getFragmentManager().beginTransaction()
                                        .replace(R.id.frameContainer,categoryFragment);
                                AlertMessage.getInstance(getContext())
                                        .ShowMessage("Informacion",res.getMessage(),transaction);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                pb_categoryItem.setVisibility(View.INVISIBLE);
                                if(error.networkResponse!=null){
                                    try {
                                        String s=new String(error.networkResponse.data,
                                                HttpHeaderParser.parseCharset(error.networkResponse.headers));
                                        com.app.expensesapp.Models.Response _Response=gson
                                                .fromJson(s, com.app.expensesapp.Models.Response.class);
                                        AlertMessage.getInstance(getContext())
                                                .ShowMessage("Informacion",_Response.getMessage());
                                    } catch (UnsupportedEncodingException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                                else {
                                    AlertMessage.getInstance(getContext())
                                            .ShowMessage("Informacion",error.toString());
                                }
                            }
                        });

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                VolleySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
            }
        });
    }

    @Override
    public boolean ValidateFields() {
        if(!txt_categoryName.getText().toString().equals("") &&
            !txt_categoryDescription.getText().toString().equals(""))return true;
        else return false;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
