package com.app.expensesapp.Fragments;

import android.content.Context;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.app.expensesapp.Interface.IApiService;
import com.app.expensesapp.Interface.IFields;
import com.app.expensesapp.Models.User;
import com.app.expensesapp.R;
import com.app.expensesapp.SQLite.SQLiteConnection;
import com.app.expensesapp.SQLite.Schema;
import com.app.expensesapp.Util.AlertMessage;
import com.app.expensesapp.Util.Config;
import com.app.expensesapp.Util.Connectivity;
import com.app.expensesapp.Util.VolleySingleton;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UserFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UserFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserFragment extends Fragment implements
        IFields {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Components
     */
    private Button btn_saveUser;
    private ProgressBar pb_updateUser;
    private EditText txt_userEdit;
    private TextView tv_titleField;

    /**
     * Objects
     */
    private SQLiteConnection connection;
    private User user;
    private int idUser;
    private NetworkInfo networkInfo;

    /**
     * Fragments
     */
    private FragmentTransaction transaction;
    private ProfileFragment profileFragment;

    public UserFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UserFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UserFragment newInstance(String param1, String param2) {
        UserFragment fragment = new UserFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_user, container, false);

        Bundle bundle=getArguments();
        String key=bundle.getString("key");
        String val=bundle.getString("val");

        InitComponents(view);
        ButtonEvents();
        tv_titleField.setText(key);
        txt_userEdit.setText(val);

        connection=new SQLiteConnection(getContext(), Schema.Database_Name,null,1);
        user=connection.SelectUser(connection);
        idUser=user.getUser_Id();
        return view;
    }

    public void UpdateLocal(User user){

        connection=new SQLiteConnection(getContext(),Schema.Database_Name,null,1);
        boolean result=false;
        if(tv_titleField.getText().toString().equals(Config.getInstance(getContext()).getTextFieldName()))
            result=connection.UpdateUserName(connection,user);
        else if(tv_titleField.getText().toString().equals(Config.getInstance(getContext()).getTextFieldLastName()))
            result=connection.UpdateUserLastName(connection,user);
        else if(tv_titleField.getText().toString().equals(Config.getInstance(getContext()).getTextFieldEmail()))
            result=connection.UpdateUserEmail(connection,user);
        else if(tv_titleField.getText().toString().equals(Config.getInstance(getContext()).getTextFieldPassword()))
            result=connection.UpdateUserPassword(connection,user);

        if(result){
            connection=new SQLiteConnection(getContext(), Schema.Database_Name,null,1);
            user=connection.SelectUser(connection);
            idUser=user.getUser_Id();
        }
        else {
            AlertMessage.getInstance(getContext()).ShowMessage("Informacion","No se pudo editar");
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void Put(String url) {
        StringRequest stringRequest=new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pb_updateUser.setVisibility(View.INVISIBLE);
                        Gson gson=new Gson();
                        com.app.expensesapp.Models.Response res=gson.fromJson(response, com.app.expensesapp.Models.Response.class);
                        UpdateLocal(user);
                        profileFragment=new ProfileFragment();
                        transaction=getFragmentManager().beginTransaction()
                                .replace(R.id.frameContainer,profileFragment);
                        AlertMessage.getInstance(getContext()).ShowMessage("Informacion",res.getMessage(),transaction);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pb_updateUser.setVisibility(View.INVISIBLE);
                        Gson gson=new Gson();
                        try {
                            String s=new String(error.networkResponse.data,
                                    HttpHeaderParser.parseCharset(error.networkResponse.headers));
                            com.app.expensesapp.Models.Response _Response=gson
                                    .fromJson(s, com.app.expensesapp.Models.Response.class);
                            AlertMessage.getInstance(getContext())
                                    .ShowMessage("Informacion",_Response.getMessage());
                        } catch (UnsupportedEncodingException e1) {
                            e1.printStackTrace();
                        }
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map=new HashMap<>();
                map.put("User_Id",String.valueOf(user.getUser_Id()));
                if(tv_titleField.getText().toString().equals(Config.getInstance(getContext()).getTextFieldName())) {
                    map.put("Name",user.getName());
                    map.put("LastName","");
                    map.put("Email","");
                    map.put("Password","");
                }
                else if(tv_titleField.getText().toString().equals(Config.getInstance(getContext()).getTextFieldLastName())) {
                    map.put("Name","");
                    map.put("LastName",user.getLastName());
                    map.put("Email","");
                    map.put("Password","");
                }
                else if(tv_titleField.getText().toString().equals(Config.getInstance(getContext()).getTextFieldEmail())) {
                    map.put("Name","");
                    map.put("LastName","");
                    map.put("Email",user.getEmail());
                    map.put("Password","");
                }
                else if(tv_titleField.getText().toString().equals(Config.getInstance(getContext()).getTextFieldPassword())) {
                    map.put("Name","");
                    map.put("LastName","");
                    map.put("Email","");
                    map.put("Password",user.getPassword());
                }
                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
    }

    @Override
    public void CleanFields() {

    }

    @Override
    public void InitComponents() {

    }

    @Override
    public void InitComponents(View view) {
        btn_saveUser=(Button) view.findViewById(R.id.btn_saveUser);
        pb_updateUser=(ProgressBar)view.findViewById(R.id.pb_updateUser);
        txt_userEdit=(EditText)view.findViewById(R.id.txt_userEdit);
        tv_titleField=(TextView)view.findViewById(R.id.tv_titleField);
    }

    @Override
    public void ButtonEvents() {
        btn_saveUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                user=new User();
                user.setUser_Id(idUser);

                String url="";
                pb_updateUser.setVisibility(View.VISIBLE);

                if(tv_titleField.getText().toString().equals(Config.getInstance(getContext()).getTextFieldName())){
                    url+=Config.getInstance(getContext()).getUrlWsUpdateUserName();
                    user.setName(txt_userEdit.getText().toString());
                    if(!user.validateName()){
                        Toast.makeText(getContext(),"El nombre esta vacio",Toast.LENGTH_LONG)
                                .show();
                        return;
                    }
                }
                else if(tv_titleField.getText().toString().equals(Config.getInstance(getContext()).getTextFieldLastName())){
                    url+=Config.getInstance(getContext()).getUrlWsUpdateUserLastName();
                    user.setLastName(txt_userEdit.getText().toString());
                    if(!user.validateLastName()){
                        Toast.makeText(getContext(),"El apellido esta vacio",Toast.LENGTH_LONG)
                                .show();
                        return;
                    }
                }
                else if(tv_titleField.getText().toString().equals(Config.getInstance(getContext()).getTextFieldEmail())){
                    url+=Config.getInstance(getContext()).getUrlWsUpdateUserEmail();
                    user.setEmail(txt_userEdit.getText().toString());
                    if(user.validateEmail()){
                        Toast.makeText(getContext(),"El correo esta vacio",Toast.LENGTH_LONG)
                                .show();
                        return;
                    }
                }
                else if(tv_titleField.getText().toString().equals(Config.getInstance(getContext()).getTextFieldPassword())){
                    url+=Config.getInstance(getContext()).getUrlWsUpdateUserPassword();
                    user.setPassword(txt_userEdit.getText().toString());
                    if(!user.validatePassword()){
                        Toast.makeText(getContext(),"El nombre esta vacio",Toast.LENGTH_LONG)
                                .show();
                        return;
                    }
                }

                networkInfo= Connectivity.getInstance(getContext()).CheckInternetConnection();
                if(networkInfo==null || !networkInfo.isConnected()){
                    AlertMessage.getInstance(getContext())
                            .ShowMessage("Informacion",Config.getInstance(getContext())
                                    .getMessageNoInternetConnection());
                    return;
                }
                pb_updateUser.setVisibility(View.VISIBLE);
                url=url.replace("{id}",String.valueOf(user.getUser_Id()));
                Put(url);
            }
        });

    }

    @Override
    public boolean ValidateFields() {
        return false;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
