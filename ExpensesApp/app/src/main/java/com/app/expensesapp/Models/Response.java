package com.app.expensesapp.Models;

public class Response {
    public boolean IsSuccess;
    public String Message;
    public Object Result;

    public Response() {
    }

    public Response(boolean isSuccess, String message, Object result) {
        this.IsSuccess = isSuccess;
        Message = message;
        Result = result;
    }

    public boolean IsSuccess() {
        return IsSuccess;
    }

    public void setSuccess(boolean success) {
        IsSuccess = success;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public Object getResult() {
        return Result;
    }

    public void setResult(Object result) {
        Result = result;
    }
}
