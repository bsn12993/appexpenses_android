package com.app.expensesapp.Activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.app.expensesapp.Fragments.CategoryFragment;
import com.app.expensesapp.Fragments.CategoryItemFragment;
import com.app.expensesapp.Fragments.ExpenseItemFragment;
import com.app.expensesapp.Fragments.ExpensesFragment;
import com.app.expensesapp.Fragments.HomeFragment;
import com.app.expensesapp.Fragments.IncomeItemFragment;
import com.app.expensesapp.Fragments.IncomesFragment;
import com.app.expensesapp.Fragments.ProfileFragment;
import com.app.expensesapp.Fragments.UserFragment;
import com.app.expensesapp.Interface.IFields;
import com.app.expensesapp.Models.User;
import com.app.expensesapp.R;
import com.app.expensesapp.SQLite.SQLiteConnection;
import com.app.expensesapp.SQLite.SQLiteManager;
import com.app.expensesapp.SQLite.Schema;
import com.app.expensesapp.Util.SharedPreferencesUser;
import com.google.gson.Gson;


public class MainActivity extends AppCompatActivity
        implements CategoryFragment.OnFragmentInteractionListener,
        ExpensesFragment.OnFragmentInteractionListener,
        IncomesFragment.OnFragmentInteractionListener,
        ProfileFragment.OnFragmentInteractionListener,
        HomeFragment.OnFragmentInteractionListener,
        ExpenseItemFragment.OnFragmentInteractionListener,
        CategoryItemFragment.OnFragmentInteractionListener,
        IncomeItemFragment.OnFragmentInteractionListener,
        UserFragment.OnFragmentInteractionListener,
        IFields
{

    /**
     * Components
     */
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;
    private TextView txt_fullName;
    private TextView txt_email;
    private TextView txt_iduser;

    /**
     * Fragments
     */
    private CategoryFragment categoryFragment;
    private ExpensesFragment expensesFragment;
    private IncomesFragment incomesFragment;
    private ProfileFragment profileFragment;
    private HomeFragment homeFragment;

    /**
     * Objetos
     */
    private User user;
    private Gson gson;
    private SQLiteConnection sqLiteConnection;
    private SharedPreferencesUser preferencesUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle(R.string.title_home);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        InitComponents();

        //preferencesUser=new SharedPreferencesUser(this);
        gson=new Gson();
        Intent bundle=getIntent();
        String json=bundle.getStringExtra("json");

        //User user1=preferencesUser.getPreferences();
        //Log.d("Id",user1.getUser_Id()+"");
        //Log.d("email",user1.getEmail());

        sqLiteConnection=new SQLiteConnection(this, Schema.Database_Name,null,1);
        user = sqLiteConnection.SelectUser(sqLiteConnection);
        if(user==null){
            user = gson.fromJson(json,User.class);
            SQLiteDatabase database=sqLiteConnection.getWritableDatabase();
            ContentValues values= SQLiteManager.setContentValues(user);
            database.insert(Schema.Table_Name,Schema.ID,values);
            database.close();
        }

        ButtonEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(toggle.onOptionsItemSelected(item))return true;
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void CleanFields() {

    }

    @Override
    public void InitComponents() {
        drawerLayout=(DrawerLayout)findViewById(R.id.drawerLayout);
        toggle=new ActionBarDrawerToggle(this,drawerLayout,R.string.Open,R.string.Close);
        toggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        navigationView=(NavigationView)findViewById(R.id.navView);
        View view= LayoutInflater.from(this).inflate(R.layout.nav_header,null);

        txt_fullName=(TextView)view.findViewById(R.id.txt_UserNameHeader);
        txt_email=(TextView)view.findViewById(R.id.txt_UserEmailHeader);

        txt_fullName.setText("aaa");
        txt_email.setText("bbb");

        homeFragment=new HomeFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.frameContainer,homeFragment).commit();
    }

    @Override
    public void InitComponents(View view) {

    }

    @Override
    public void ButtonEvents() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
                switch (menuItem.getItemId()){
                    case R.id.op_categories:
                        categoryFragment=new CategoryFragment();
                        transaction.replace(R.id.frameContainer,categoryFragment).addToBackStack(null);
                        setTitle(R.string.title_category);
                        break;
                    case R.id.op_expenses:
                        expensesFragment=new ExpensesFragment();
                        transaction.replace(R.id.frameContainer,expensesFragment).addToBackStack(null);
                        setTitle(R.string.title_expenses);
                        break;
                    case R.id.op_incomes:
                        incomesFragment=new IncomesFragment();
                        transaction.replace(R.id.frameContainer,incomesFragment).addToBackStack(null);
                        setTitle(R.string.title_incomes);
                        break;
                    case R.id.op_profile:
                        profileFragment=new ProfileFragment();
                        transaction.replace(R.id.frameContainer,profileFragment).addToBackStack(null);
                        setTitle(R.string.title_profile);
                        break;
                    case  R.id.op_home:
                        homeFragment=new HomeFragment();
                        transaction.replace(R.id.frameContainer,homeFragment).addToBackStack(null);
                        setTitle(R.string.title_home);
                    case R.id.op_exit:
                        sqLiteConnection=new SQLiteConnection(getApplicationContext(),Schema.Database_Name,null,1);
                        boolean delete=sqLiteConnection.DeleteUser(sqLiteConnection,1);
                        Intent iLogin=new Intent(MainActivity.this,LoginActivity.class);
                        startActivity(iLogin);
                        break;
                }
                transaction.commit();
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            }
        });
    }

    @Override
    public boolean ValidateFields() {
        return false;
    }
}
