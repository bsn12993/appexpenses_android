package com.app.expensesapp.Activities;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.app.expensesapp.Interface.IFields;
import com.app.expensesapp.Models.Response;
import com.app.expensesapp.Models.User;
import com.app.expensesapp.R;
import com.app.expensesapp.Util.AlertMessage;
import com.app.expensesapp.Util.Config;
import com.app.expensesapp.Util.Security;
import com.app.expensesapp.Util.Util;
import com.app.expensesapp.Util.VolleySingleton;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity implements
        IFields{

    /**
     * Components
     */
    private Button btn_entrar;
    private Button btn_userRegister;
    private EditText txt_name;
    private EditText txt_lastName;
    private EditText txt_email;
    private EditText txt_password;
    private ProgressBar pb_register;

    /**
     * Objetos
     */
    private JsonObjectRequest postRequest;
    private Gson gson;
    private Response _Response;
    private User user;
    private android.app.ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        setTitle(R.string.title_register);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        gson=new Gson();

        InitComponents();
        ButtonEvents();
    }

    public void Post(String url) {
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pb_register.setVisibility(View.INVISIBLE);
                _Response = gson.fromJson(response,Response.class);
                 CleanFields();
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pb_register.setVisibility(View.INVISIBLE);
                try {
                    String s=new String(error.networkResponse.data,
                            HttpHeaderParser.parseCharset(error.networkResponse.headers));
                    _Response=gson.fromJson(s,Response.class);
                    AlertMessage.getInstance(getApplicationContext())
                            .ShowMessage("Informacion",_Response.getMessage());
                } catch (UnsupportedEncodingException e1) {
                    e1.printStackTrace();
                }
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("Name",user.getName());
                params.put("LastName",user.getLastName());
                params.put("Email",user.getEmail());
                params.put("Password",user.getPassword());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    @Override
    public void CleanFields() {
        user.ClearFields();
        txt_name.setText(user.getName());
        txt_lastName.setText(user.getLastName());
        txt_email.setText(user.getEmail());
        txt_password.setText(user.getPassword());
    }

    @Override
    public void InitComponents() {
        pb_register=(ProgressBar)findViewById(R.id.pb_register);
        txt_name=(EditText)findViewById(R.id.txt_name);
        txt_lastName=(EditText)findViewById(R.id.txt_lastname);
        txt_email=(EditText)findViewById(R.id.txt_email);
        txt_password=(EditText)findViewById(R.id.txt_password);
        btn_entrar=(Button)findViewById(R.id.btn_entrar);
        btn_userRegister=(Button)findViewById(R.id.btn_userRegister);
    }

    @Override
    public void InitComponents(View view) {

    }

    @Override
    public void ButtonEvents() {
        btn_entrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iLogin=new Intent(RegisterActivity.this,LoginActivity.class);
                startActivity(iLogin);
            }
        });

        btn_userRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ProgressDialogMessage.getInstance(getApplicationContext()).showProgressDialog("Consultando").show();
                if(!ValidateFields()){
                    Toast.makeText(getApplicationContext(),"Campos vacios",Toast.LENGTH_LONG)
                            .show();
                    return;
                }
                user=new User();
                user.setName(txt_name.getText().toString());
                user.setLastName(txt_lastName.getText().toString());
                user.setEmail(txt_email.getText().toString());
                user.setPassword(Security.getCifrado(txt_password.getText().toString(),"SHA1"));
                pb_register.setVisibility(View.VISIBLE);
                Post(Config.getInstance(getApplicationContext()).getUrlWSUserCreate());
            }
        });
    }

    @Override
    public boolean ValidateFields() {
        if(!txt_name.getText().toString().equals("") &&
            !txt_lastName.getText().toString().equals("") &&
            !Util.ValidateEmail(txt_email.getText().toString()) &&
            !txt_password.getText().toString().equals(""))return true;
        else return false;
    }
}
