package com.app.expensesapp.Services;

import com.app.expensesapp.Interface.ApiServices;
import com.app.expensesapp.Models.User;

public interface UserImpl extends ApiServices<User> {

}
