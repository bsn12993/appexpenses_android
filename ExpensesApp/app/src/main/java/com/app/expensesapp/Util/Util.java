package com.app.expensesapp.Util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util {

    public static boolean ValidateEmail(String email){
        Pattern pattern=Pattern.compile("[A-Za-z0-9-]+(\\\\.[A-Za-z0-9]+)*(\\\\.[A-Za-z]{2,})$");
        Matcher math=pattern.matcher(email);
        if(math.find())return true;
        else return false;
    }

    public static String setFormatDate(String date){
        date=date.split("T")[0];
        String[] val=date.split("-");
        return val[2]+"/"+val[1]+"/"+val[0];
    }

    public static String setFormatDateEN(String date){
        String[] val=date.split("/");
        return val[2]+"/"+val[1]+"/"+val[0];
    }
}
